
CFLAGS=-O3 -I. -lpthread -pthread -std=c++1y

all: ttrsrv ttrai

clean:
	rm bin/ttr*

ttrsrv: bin ttrapi/ttrapi.cpp ttrapi/ttrgraph.cpp ttrsrv/ttrsrv.cpp
	g++ $(CFLAGS) -o bin/ttrsrv ttrapi/ttrapi.cpp ttrapi/ttrgraph.cpp ttrsrv/ttrsrv.cpp

ttrai: bin ttrapi/ttrapi.cpp ttrapi/ttrgraph.cpp ttrai/ttrai.cpp
	g++ $(CFLAGS) -o bin/ttrai ttrapi/ttrapi.cpp ttrapi/ttrgraph.cpp ttrai/ttrai.cpp

bin:
	mkdir bin
