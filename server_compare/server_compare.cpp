#include "../ttrapi/ttrapi.h"
#include "../ttrapi/ttrgraph.h"
#include <iostream>
#include <thread>
#include <assert.h>

using namespace ttrapi;
using namespace std;

static bool compareState(const GameState& _a, const GameState& _b)
{
	assert(equal(_a.cityOwner.begin(), _a.cityOwner.end(), _b.cityOwner.begin(), _b.cityOwner.end()));
	assert(equal(_a.routeOwner.begin(), _a.routeOwner.end(), _b.routeOwner.begin(), _b.routeOwner.end()));
	return true;
}

static bool comparePlayer(const Player& _a, const Player& _b)
{
	assert(equal(_a.ticketCards.begin(), _a.ticketCards.end(), _b.ticketCards.begin(), _b.ticketCards.end(), 
		[](const TicketDef& _a, const TicketDef& _b) {
			return ((_a.city[0] == _b.city[0] && _a.city[1] == _b.city[1]) ||
				(_a.city[0] == _b.city[1] && _a.city[1] == _b.city[0]))
				&& _a.points == _b.points;
		}));

	for (size_t i = 0; i < TCC_NONE; ++i)
		assert(_a.trainCards[i] == _b.trainCards[i]);
	return true;
}

static void aiThink(shared_ptr<Connection> _conn1, shared_ptr<Connection> _conn2)
{
	Token token(_conn1);
	Token token2(_conn2);
	compareState(_conn1->gameState(), _conn2->gameState());
	comparePlayer(_conn1->playerState(), _conn2->playerState());

	if (_conn1->currentRound() == 0)
	{
		token.drawDestinationTickets();
		token2.drawDestinationTickets();
		return;
	}

	const TicketDef* ticket = nullptr;
	vector<Route*> ticketPath;

	Map<> net;
	net.updateOwner(_conn1->gameState());
	for (auto& it : _conn1->playerState().ticketCards)
	{
		vector<CityLink*> path = net.dijkstra(it.city[0], it.city[1], [&](const CityLink& _l) {
			if (_l.route.owner && _l.route.owner->id == _conn1->playerId())
				return 0;
			else if (!_l.route.canBuild(_conn1->playerState()))
				return -1;
			else
				return (int)_l.route.def->length;
		});
		for (auto it : path)
			ticketPath.push_back(&it->route);
	}

	bool allDone = true;
	for (auto it : ticketPath)
		allDone &= it->owner != nullptr;

	if (allDone)
	{
		token.drawDestinationTickets();
		token2.drawDestinationTickets();
		return;
	}

	size_t numRoutes = ticketPath.size();
	for (size_t i = 0; i < numRoutes; ++i)
		if (ticketPath[i]->twin)
			ticketPath.push_back(ticketPath[i]->twin);

	for (auto& l : ticketPath)
	{
		TrainCardColor color = l->canBuild(_conn1->playerState(), l->def->tunnle ? 1 : 0);
		if (color != TCC_NONE)
		{
			token.buildRoute(*l->def, color);
			token2.buildRoute(*l->def, color);
			return;
		}
	}

	token.drawCoverdTrainCard();
	token2.drawCoverdTrainCard();
	token.drawCoverdTrainCard();
	token2.drawCoverdTrainCard();
}

int main(int _argc, char* _argv[])
{
	try
	{
		shared_ptr<Connection> conn1 = make_shared<Connection>(_argv[1], "127.0.0.1", 13000);
		shared_ptr<Connection> conn2 = make_shared<Connection>(_argv[1], "127.0.0.1", 13001);

		while (true)
		{
			try {
				aiThink(conn1, conn2);
			}
			catch (rule_violation e)
			{
				cerr << "rule violation " << e.what() << endl;
			}
		}


	}
	catch (connection_failure e)
	{
		cerr << "network error: " << e.what() << endl;
	}

	return 0;
}
