#include "../ttrapi/ttrapi.h"
#include "../ttrapi/ttrgraph.h"
#include <iostream>
#include <thread>


using namespace ttrapi;
using namespace std;

static void aiThink(shared_ptr<Connection> _conn)
{
	Token token(_conn);

	if (_conn->currentRound() == 0)
	{
		token.drawDestinationTickets();
		return;
	}

	const TicketDef* ticket = nullptr;
	vector<Route*> ticketPath;

	Map<> net;
	net.updateOwner(_conn->gameState());
	for (auto& it : _conn->playerState().ticketCards)
	{
		vector<CityLink*> path = net.dijkstra(it.city[0], it.city[1], [&](const CityLink& _l) {
			if (_l.route.owner && _l.route.owner->id == _conn->playerId())
				return 0;
			else if (!_l.route.available())
				return -1;
			else
				return 1;
		});

		for (auto it : path)
			ticketPath.push_back(&it->route);
	}

	bool allDone = true;
	for (auto it : ticketPath)
		allDone &= it->owner != nullptr;

	if (allDone)
	{
		token.drawDestinationTickets();
		return;
	}

	size_t numRoutes = ticketPath.size();
	for (size_t i = 0; i < numRoutes; ++i)
		if (ticketPath[i]->twin)
			ticketPath.push_back(ticketPath[i]->twin);

	for (auto& l : ticketPath)
	{
		TrainCardColor color = l->canBuild(_conn->playerState(), l->def->tunnle ? 1 : 0);
		if (color != TCC_NONE)
		{
			token.buildRoute(*l->def, color);
			return;
		}
	}

	token.drawCoverdTrainCard();
	token.drawCoverdTrainCard();
}

int main()
{
	try
	{
		shared_ptr<Connection> conn = make_shared<Connection>("Player1");

		while (true)
		{
			try {
				aiThink(conn);
			}
			catch (rule_violation e)
			{
				cerr << "rule violation " << e.what() << endl;
			}
		}


	}
	catch (connection_failure e)
	{
		cerr << "network error: " << e.what() << endl;
	}

	return 0;
}
