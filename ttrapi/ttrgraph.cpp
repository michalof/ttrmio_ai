#include "ttrgraph.h"

using namespace std;
using namespace ttrapi;


CityLink * ttrapi::City::link(CityName _dest)
{
	for (auto& it : routes)
		if (it.dest.name == _dest)
			return &it;
	return nullptr;
}

bool ttrapi::City::canBuild(const Player & _player) const
{
	size_t bestCount = 0;
	TrainCardColor bestColor = TCC_LOCO;

	for (size_t i = 0; i < TCC_NONE; ++i)
	{
		if (_player.trainCards[i] > bestCount)
		{
			bestCount = _player.trainCards[i];
			bestColor = (TrainCardColor)i;
		}
	}
	return canBuild(_player, bestColor);
}

bool ttrapi::City::canBuild(const Player& _player, TrainCardColor _color) const
{
	if (this->owner || _player.numStationTokens == 0 || _color >= TCC_NONE)
		return false;
	
	size_t cost = (TOKEN_STATION + 1) - _player.numStationTokens;
	size_t playerCards = _player.trainCards[_color];
	if (_color != TCC_LOCO) playerCards += _player.trainCards[TCC_LOCO];

	return playerCards >= cost;
}

bool ttrapi::City::build(Player & _player, TrainCardColor _color, ServerGameState & _state)
{
	if (!canBuild(_player, _color))
		return false;

	_state.move.type = MT_BuildStation;
	_state.move.build.station = name;

	size_t cost = TOKEN_STATION + 1 - _player.numStationTokens;
	_state.pay(_player, _color, cost);
	_player.numStationTokens--;
	_state.cityOwner[name] = (uint8_t)_player.id;
	owner = &_player;
	return true;
}

bool ttrapi::Route::available() const
{
	return !owner && (!twin || map->allowDoubleTracks() || !twin->owner);
}

bool ttrapi::Route::build(Player & _player, TrainCardColor _color, size_t _extraCost, ServerGameState & _state)
{
	_state.move.type = MT_BuildRoute;
	_state.move.build.route = def;

	if (canBuild(_player, _color, _extraCost))
	{
		_state.pay(_player, _color, def->length + _extraCost, def->locos);
		_player.numTrainTokens -= def->length;
		_state.routeOwner[def->id - 1] = (uint8_t)_player.id;
		owner = &_player;
		return true;
	}
	else
	{
		return false;
	}
}

TrainCardColor ttrapi::Route::canBuild(const Player & _player, size_t _extraCost) const
{
	if (!available())
		return TCC_NONE;

	if (_player.numTrainTokens < def->length)
		return TCC_NONE;

	if (_player.trainCards[TCC_LOCO] < def->locos)
		return TCC_NONE;

	if (twin && twin->owner && (!map->allowDoubleTracks() || twin->owner->id == _player.id))
		return TCC_NONE;

	if (def->color != TCC_ALL)
	{
		size_t cardsAvailable = _player.trainCards[def->color] + _player.trainCards[TCC_LOCO];
		return cardsAvailable >= def->length + _extraCost ? def->color : TCC_NONE;
	}

	for (size_t i = 1; i < TCC_NONE; ++i)
	{
		size_t cardsAvailable = _player.trainCards[i] + _player.trainCards[TCC_LOCO];
		if (_player.trainCards[i] && cardsAvailable >= _extraCost + def->length)
			return (TrainCardColor)i;
	}

	if (_player.trainCards[TCC_LOCO] >= def->length + _extraCost)
		return TCC_LOCO;
	return TCC_NONE;
}

bool ttrapi::Route::canBuild(const Player & _player, TrainCardColor _color, size_t _extraCost) const
{
	if (owner || _player.numTrainTokens < def->length || _color >= TCC_NONE)
		return false;

	if (twin && twin->owner && (!map->allowDoubleTracks() || twin->owner->id == _player.id))
		return false;

	if (def->color != TCC_ALL && def->color != _color)
		return false;

	if (_player.trainCards[TCC_LOCO] < def->locos)
		return false;

	size_t cost = def->length + _extraCost;
	size_t playerCards = _player.trainCards[_color];
	if (_color != TCC_LOCO) playerCards += _player.trainCards[TCC_LOCO];

	return playerCards >= cost;
}

bool ttrapi::Route::passable(const Player & _player) const
{
	if (owner && owner->id == _player.id)
		return true;
	for (const City* c : city)
		if (c->owner && c->owner->id == _player.id)
			return true;
	return false;
}
