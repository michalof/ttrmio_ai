#define _CRT_SECURE_NO_WARNINGS
#include "ttrapi.h"
#include "ttrgraph.h"

#include <iostream>
#include <sstream>
#include <utility>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"

#ifdef _WIN32
#include <WinSock2.h>
#include <ws2tcpip.h>
#undef GetObject
#undef min
#undef max

static WSAData g_wsa;
static bool g_wsaInit = false;

static void initNetwork()
{
	if (!g_wsaInit)
	{
		g_wsaInit = true;
		WSAStartup(MAKEWORD(2, 2), &g_wsa);
	}
}

#pragma comment (lib, "WS2_32.lib")
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <strings.h>
#include <unistd.h>

#define _stricmp strcasecmp
#define closesocket ::close
#define ADDR_ANY INADDR_ANY

static void initNetwork() {}
#endif

using namespace std;

static std::vector<std::string> explode(std::string const & s, char delim)
{
	std::vector<std::string> result;
	std::istringstream iss(s);

	for (std::string token; std::getline(iss, token, delim); )
	{
		result.push_back(std::move(token));
	}

	return result;
}

static ttrapi::CityName findCity(const char* _name)
{
	for (size_t i = 0; i < ttrapi::CN_NONE; ++i)
	{
		if (_stricmp(ttrapi::g_CityNames[i], _name) == 0)
			return (ttrapi::CityName)i;
	}
	return ttrapi::CN_NONE;
}


ttrapi::Connection::Connection(Pipe * _pipe, size_t _myId)
{
	m_Pipe = _pipe;
	m_PlayerId = _myId;
}

ttrapi::Connection::Connection(std::string _playerName, const char * _hostname, uint16_t _port) : m_PlayerId(0)
{
	connect(_playerName, _hostname, _port);
}

ttrapi::Connection::~Connection()
{
	closesocket(m_Sock);
}

void ttrapi::Connection::send(const char * _cmd)
{
	string cmd = m_Hash + ' ' + _cmd + '\n';

	if (m_Pipe)
	{
		m_Pipe->send(Pipe::SERVER, move(cmd));
	}
	else
	{
		if (::send(m_Sock, cmd.c_str(), (int)cmd.length(), 0) != cmd.length())
			throw connection_failure("connection dropped");
	}
}

void ttrapi::Connection::send(const ostringstream & _str)
{
	send(_str.str().c_str());
}

std::string ttrapi::Connection::recv(int _packetTypeMin, int _packetTypeMax, int* _code)
{
	for (auto it = m_Packets.begin(); it != m_Packets.end(); ++it)
	{
		if (it->first >= _packetTypeMin && it->first <= _packetTypeMax)
		{
			if (it->first == 10)
				updateGameState(it->second);
			else if (it->first == 4)
				updateTrainCards(it->second);

			if (_code)
				*_code = it->first;

			string msg = it->second;
			m_Packets.erase(it);
			return msg;
		}
	}

	while(true) 
	{
		string str_buffer;
		char buffer[10240];
		const char* msg_data = buffer;

		if (m_Pipe)
		{
			m_Pipe->recv(Pipe::CLIENT, str_buffer);
			msg_data = str_buffer.c_str();
		}
		else
		{
			size_t pos = 0;
			do {
				if (::recv(m_Sock, buffer + pos, 1, 0) != 1)
					throw connection_failure("connection dropped");

				++pos;
			} while (buffer[pos - 1] != '\n');
			buffer[--pos] = 0;
		}

		int code = atoi(msg_data);
		const char* msgStart = strchr(msg_data, ' ');
		string msg;
		if (msgStart)
			msg = msgStart + 1;

		if (code == -1)
			throw rule_violation("rule violation packet from server");
		else if (code == -3)
			throw timeout();
		else if (code == 100)
			throw connection_failure("disconnected");
		if (code < 0)
			throw connection_failure("error response from server");

		if (code >= _packetTypeMin && code <= _packetTypeMax)
		{
			if (code == 10)
				updateGameState(msg);
			else if (code == 4)
				updateTrainCards(msg);

			if (_code)
				*_code = code;

			return msg;
		}
		m_Packets.push_back(make_pair(code, msg));
	}
	return std::string();
}

void ttrapi::Connection::connect(std::string & _playerName, const char * _hostname, uint16_t _port)
{
	initNetwork();

	char portString[8];
	sprintf(portString, "%i", (int)_port);

	addrinfo hint = {};
	hint.ai_socktype = SOCK_STREAM;

	addrinfo* addr;
	if (getaddrinfo(_hostname, portString, &hint, &addr) != 0)
		throw connection_failure("unknown host");

	for (addrinfo* it = addr; it; it = it->ai_next)
	{
		m_Sock = (unsigned int)socket(it->ai_family, it->ai_socktype, it->ai_protocol);
		if (::connect(m_Sock, it->ai_addr, (int)it->ai_addrlen) == 0)
			break;

		closesocket(m_Sock);
		m_Sock = 0;
	}
	freeaddrinfo(addr);

	if (m_Sock == 0)
		throw connection_failure("connection failed");

	_playerName += '\n';
	if (::send(m_Sock, _playerName.c_str(), (int)_playerName.length(), 0) != _playerName.length())
		throw connection_failure("sending name failed");

	char buffer[8];
	int nameLen = ::recv(m_Sock, buffer, 7, 0);
	if (nameLen != 2)
		throw connection_failure("expected player id");

	m_PlayerId = buffer[0] - '0';
}

void ttrapi::Connection::updateGameState(const std::string & _jsonState)
{
	m_State.round++;

	using namespace rapidjson;

	const char* jsonSplit = strchr(_jsonState.c_str(), ' ');
	if (!jsonSplit)
		throw connection_failure("invalid json packet");

	Document d;
	// parse game state
	d.Parse(jsonSplit + 1);

	m_State.player.resize(d["playerCount"].GetInt());
	m_State.timeout = d["timeout"].GetFloat();
	m_State.availableTrainCards = d["trainCardCount"].GetInt();
	m_State.availableTicketCards = d["destinationTicketCount"].GetInt();

	if (d["seed"].IsInt())
		m_State.aiSeed = d["seed"].GetInt();
	
	auto openCardIt = m_State.openTrainCards.begin();
	for (auto& it : d["openTrainCardSpots"].GetArray())
		*openCardIt++ = (TrainCardColor)it.GetInt();

	m_State.droppedTrainCards.fill(0);
	for (auto& it : d["droppedTrainCards"].GetArray())
		m_State.droppedTrainCards[it.GetInt()]++;

	for (auto& it : d["builtRoutes"].GetObject())
		m_State.routeOwner[atoi(it.name.GetString()) - 1] = it.value.GetInt();

	for (auto& it : d["builtStations"].GetObject())
	{
		for(size_t i = 0; i < CN_NONE; ++i)
			if (_stricmp(g_CityNames[i], it.name.GetString()) == 0)
			{
				m_State.cityOwner[i] = it.value.GetInt();
				break;
			}
	}
	
	for (auto& it : d["log"].GetArray())
	{
		Move m;
		m.round = it["round"].GetInt();
		m.playerId = it["playerID"].GetInt();

		if (!m_State.log.empty() && 
			m.playerId + m.round * m_State.player.size() <= 
			m_State.log.back().playerId + m_State.log.back().round * m_State.player.size())
			continue;

		m.type = (MoveType)it["turn"].GetInt();

		if (m.type == MT_BuildRoute || m.type == MT_BuildStation)
		{
			m.build.color = (TrainCardColor)it["trainCardColor1"].GetInt();
			m.build.numTrainCards = it["card1Count"].GetInt();
			m.build.numLocoCards = it["card2Count"].GetInt();
			m.build.success = it["success"].GetBool();

			if (it.HasMember("additional1Cards"))
				m.build.numTrainCards += it["additional1Cards"].GetInt();
			if (it.HasMember("additional2Cards"))
				m.build.numLocoCards += it["additional2Cards"].GetInt();

			if (m.type == MT_BuildStation)
			{
				m.build.station = (CityName)it["city"].GetInt();
			}
			else
			{
				m.build.route = &g_Routes[it["route"]["id"].GetInt() - 1];
			}
		}
		else if (m.type == MT_DrawTrainCards)
		{
			for (int i = 0; i < it["card1Count"].GetInt(); ++i)
				m.trainsDrawn.push_back((TrainCardColor)it["trainCardColor1"].GetInt());
			for (int i = 0; i < it["card2Count"].GetInt(); ++i)
				m.trainsDrawn.push_back((TrainCardColor)it["trainCardColor2"].GetInt());
		}
		else if (m.type == MT_DrawDestinationTickets)
		{
			m.ticketsDrawn = it["card1Count"].GetInt();
		}

		m_State.log.push_back(move(m));
	}
	
	// parse player objects
	d.Parse(_jsonState.c_str(), jsonSplit - _jsonState.c_str());

	if (m_State.player.empty())
		m_State.player.resize(d.Size());

	for (auto& src : d.GetArray())
	{
		Player& p = m_State.player[src["id"].GetInt()];
		p.id = src["id"].GetInt();

		p.numStationTokens = src["stationCount"].GetInt();
		p.numTrainTokens = src["trainPieceCount"].GetInt();
		p.numTrainCards = src["totalTrainCardCount"].GetInt();
		p.numTicketCards = src["totalDestinationTicketCount"].GetInt();

		if (p.id == m_PlayerId)
		{
			m_Hash = src["hash"].GetString();
			
			for (uint8_t& it : p.trainCards)
				it = 0;
			for (auto& it : src["trainCardsDictionary"].GetObject())
			{
				int id = it.name.GetString()[0] - '0';
				if (id >= 0 && id < TCC_NONE)
				{
					p.trainCards[id] = it.value.GetInt();
				}
				else
				{
					for (size_t i = 0; i < TCC_NONE; ++i)
					{
						if (_stricmp(g_ColorNames[i], it.name.GetString()) == 0)
						{
							p.trainCards[i] = it.value.GetInt();
							break;
						}
					}
				}
			}

			p.ticketCards.clear();
			for (auto& it : src["destinationTickets"].GetArray())
			{
				TicketDef t;
				t.city[0] = (CityName)it["destination1"].GetInt();
				t.city[1] = (CityName)it["destination2"].GetInt();
				t.points = it["points"].GetInt();
				p.ticketCards.push_back(t);
			}
		}
	}
}

void ttrapi::Connection::updateTrainCards(const std::string & _cards)
{
	auto colors = explode(_cards, ' ');

	for (size_t i = 0; i < colors.size(); ++i)
	{
		TrainCardColor color = TCC_NONE;
		for (size_t j = 0; j < TCC_ALL; ++j)
			if (_stricmp(g_ColorNames[j], colors[i].c_str()) == 0)
			{
				color = (TrainCardColor)j;
				break;
			}
		if (i < 5)
			m_State.openTrainCards[i] = color;
		else if (i == 5 && color < TCC_NONE)
			m_State.player[m_PlayerId].trainCards[color]++;
	}
}

ttrapi::Token::Token(std::shared_ptr<Connection> _conn) : m_Conn(_conn)
{
	m_Conn->recv(10);
	m_Timeout = m_Conn->gameState().timeout;
	m_StartTime = chrono::system_clock::now();
}

float ttrapi::Token::timeLeft() const
{
	auto timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(chrono::system_clock::now() - m_StartTime).count();
	return m_Timeout - (float)timeDiff / 1000;
}

void ttrapi::Token::drawCoverdTrainCard()
{
	if (!m_Conn)
		throw rule_violation("token invalid");

	m_Conn->send("CoTC");
	m_Conn->recv(4);
}

void ttrapi::Token::drawOpenTrainCard(size_t _spot)
{
	if (!m_Conn)
		throw rule_violation("token invalid");

	ostringstream cmd;
	cmd << "OpTC " << _spot;
	m_Conn->send(cmd);
	m_Conn->recv(4);
}

void ttrapi::Token::drawDestinationTickets(std::function<void(std::vector<TicketDecision>&_tickets)> _callback)
{
	if (!m_Conn)
		throw rule_violation("token invalid");

	vector<TicketDecision> tickets;
	ostringstream cmd;

	if (m_Conn->currentRound() == 0)
	{
		for (auto& it : m_Conn->playerState().ticketCards)
		{
			TicketDecision d;
			(TicketDef&)d = it;
			tickets.push_back(d);
		}

		cmd << "DTDr";
	}
	else
	{
		m_Conn->send("DeTi");
		string msg = m_Conn->recv(6, 8);

		vector<string> ticketMsg = explode(msg, ' ');
		ticketMsg.erase(ticketMsg.begin());

		for (size_t i = 0; i < ticketMsg.size() / 3; ++i)
		{
			TicketDecision t;
			t.city[0] = findCity(ticketMsg[i * 3 + 0].c_str());
			t.city[1] = findCity(ticketMsg[i * 3 + 1].c_str());
			t.points = atoi(ticketMsg[i * 3 + 2].c_str());
			tickets.push_back(t);
		}

		cmd << "DTDe";
	}

	_callback(tickets);

	for (size_t i = 0; i < tickets.size(); ++i)
		if (tickets[i].keep)
			cmd << ' ' << i;

	m_Conn->send(cmd);
	m_Conn->recv(0);
	m_Conn.reset();
}

void ttrapi::Token::buildRoute(const RouteDef & _route, TrainCardColor _color, std::function<bool(size_t _extraCost)> _callback)
{
	if (!m_Conn)
		throw rule_violation("token invalid");

	ostringstream cmd;
	cmd << "RoBu " << (size_t)_route.id << ' ' << g_ColorNames[_color];
	m_Conn->send(cmd);

	int cost;
	m_Conn->recv(0, 3, &cost);

	if (cost)
	{
		bool build = _callback((size_t)cost);
		ostringstream cmd;
		cmd << "RoDe " << (build ? '1' : '0');
		m_Conn->send(cmd);
		m_Conn->recv(0);
	}
	m_Conn.reset();
}

void ttrapi::Token::buildStation(CityName _city, TrainCardColor _color)
{
	if (!m_Conn)
		throw rule_violation("token invalid");

	ostringstream cmd;
	cmd << "StBu " << g_CityNames[_city] << ' ' << g_ColorNames[_color];
	m_Conn->send(cmd);
	m_Conn->recv(0);
	m_Conn.reset();
}

ttrapi::GameServer::GameServer()
{
	m_State.timeout = 60.0f;
}

ttrapi::GameServer::~GameServer()
{
	close();
}

void ttrapi::GameServer::enableNetworkLogging(const char * _filename)
{
	m_NetLog.open(_filename, ios_base::trunc);
	m_NetLog << "netlog1" << endl;
}

bool ttrapi::GameServer::listen(uint16_t _port)
{
	auto sock = make_shared<SocketWrapper>();
	if (!sock->listen(_port))
		return false;
	listen(sock);
	return true;
}

std::string ttrapi::GameServer::waitForPlayer()
{
	try 
	{
		sockaddr_in addr;
		socklen_t addrLen = sizeof(addr);
		int s = (int)accept(m_Sock->socket(), (sockaddr*)&addr, &addrLen);
		if (s < 1)
			return string();

		Player p;
		p.id = m_State.player.size();

		ClientInfo info;
		info.sock = (unsigned int)s;

		ostringstream idmsg;
		idmsg << p.id << endl;
		writeMsg(info, idmsg);

		readSocketLine(info, info.name, 2000);

		m_State.player.push_back(p);
		m_Clients.push_back(info);
		return info.name;
	}
	catch(exception e)
	{
		cerr << "waitForPlayer failed: " << e.what() << endl;
	}
	return string();
}

size_t ttrapi::GameServer::addLocalPlayer(Pipe * _pipe)
{
	ClientInfo clInfo;
	clInfo.name = to_string(m_Clients.size());
	clInfo.pipe = _pipe;
	m_Clients.emplace_back(clInfo);

	Player p;
	p.id = m_State.player.size();
	m_State.player.push_back(p);
	return p.id;
}

void ttrapi::GameServer::close()
{
	m_Sock.reset();

	for (auto client : m_Clients)
	{
		if(client.sock)
			closesocket(client.sock);
	}
	m_Clients.clear();
}

bool ttrapi::GameServer::run()
{
	m_Sock.reset();
	size_t pid = m_Turn % m_State.player.size();

	try {
		try {
			if (m_TurnState == TS_IDLE)
				sendTurnMsg();
			readCmdMsg();
		}
		catch (timeout)
		{
			sendErrMsg(pid, -3);
			clientDefaultMove(pid);
		}
		catch (protocol_error)
		{
			sendErrMsg(pid, -2);
		}
		catch (rule_violation)
		{
			sendErrMsg(pid, -1);
		}
	}
	catch (exception)
	{
		clientDefaultMove(pid);
	}

	if (m_TurnState == TS_DONE)
	{
		if (m_LastTurn && m_LastTurn == m_Turn)
		{
			for (size_t i = 0; i < m_State.player.size(); ++i)
				sendErrMsg(i, 100);
			return false;
		}
		if (m_LastTurn == 0 && m_State.player[pid].numTrainTokens <= TOKEN_TRAIN_MIN)
			m_LastTurn = m_Turn + m_State.player.size();

		m_Turn++;
		m_State.round = (uint16_t)(m_Turn / m_State.player.size());
		m_TurnState = TS_IDLE;

		m_State.log.push_back(m_State.move);
		m_State.move = Move();
		m_State.move.round = m_Turn / m_State.player.size();
		m_State.move.playerId = m_Turn % m_State.player.size();
	}

	return true;
}

bool ttrapi::GameServer::aiResponsive() const
{
	if (m_State.log.size() < m_State.player.size())
		return true;

	for (size_t i = m_State.log.size() - m_State.player.size(); i < m_State.log.size(); ++i)
		if (m_State.log[i].type != MT_NoTurnWasPossible)
			return true;
	return false;
}

std::vector<int> ttrapi::GameServer::points() const
{
	vector<int> pts;

	for (auto& player : m_State.player)
		pts.push_back(m_State.points(player));

	int bestRoute = m_State.longestPathPlayer();
	if (bestRoute > 0)
		pts[bestRoute] += POINTS_FOR_LONGEST_PATH;

	return pts;
}

void ttrapi::GameServer::readSocketLine(ClientInfo& _sock, string& _buf, time_t _timeoutMs)
{
	if (_sock.pipe)
	{
		_sock.pipe->recv(Pipe::SERVER, _buf);
	}
	else
	{
		fd_set set;
		time_t startTime = time(NULL);

		do {
			FD_ZERO(&set);
			FD_SET(_sock.sock, &set);

			time_t timeleft = _timeoutMs - (time(NULL) - startTime);
			if (timeleft < 1)
				throw timeout();

			timeval timeoutVal = { (long)(timeleft / 1000), (int)(timeleft % 1000) * 1000 };
			if (::select(_sock.sock + 1, &set, nullptr, nullptr, &timeoutVal) == 0)
				throw timeout();

			char c;
			if (::recv(_sock.sock, &c, 1, 0) != 1)
				throw connection_failure("connection lost");
			_buf.push_back(c);

		} while (_buf.back() != '\n');
	}

	if (m_NetLog)
		m_NetLog << '<' << hex << setw(4) << _sock.name << ": " << _buf;

	_buf.pop_back();

	if (!_buf.empty() && _buf.back() == '\r')
		_buf.pop_back();
}

void ttrapi::GameServer::sendTurnMsg()
{
	size_t pid = m_Turn % m_State.player.size();
	m_State.updateCountVars();

	ostringstream msg;
	msg << 10 << ' ' << jsonPlayerData(pid) << ' ' << jsonGameData() << endl;
	writeMsg(m_Clients[pid], msg);

	m_TurnState = m_State.round == 0 ? TS_DEST_CARD : TS_WAIT;
	m_TurnStart = time(NULL);
}

void ttrapi::GameServer::sendErrMsg(size_t _pid, int _err)
{
	ostringstream msg;
	msg << _err << endl;
	writeMsg(m_Clients[_pid], msg);
}

void ttrapi::GameServer::readCmdMsg()
{
	size_t pid = m_Turn % m_State.player.size();

	time_t timeLeft = (time_t)(m_State.timeout * 1000) - (time(NULL) - m_TurnStart);

	string msg;
	readSocketLine(m_Clients[pid], msg, timeLeft);

	vector<string> cmd = explode(msg, ' ');
	if (cmd[0] != m_TurnHash)
	{
		sendErrMsg(pid, -3);
		return;
	}

	if (cmd[1] == "DeTi")
		clientDeTi(pid, cmd);
	else if (cmd[1] == "DTDr" || cmd[1] == "DTDe")
		clientDTDe(pid, cmd);
	else if (cmd[1] == "OpTC")
		clientOpTC(pid, cmd);
	else if (cmd[1] == "CoTC")
		clientCoTC(pid, cmd);
	else if (cmd[1] == "RoBu")
		clientRoBu(pid, cmd);
	else if (cmd[1] == "RoDe")
		clientRoDe(pid, cmd);
	else if (cmd[1] == "StBu")
		clientStBu(pid, cmd);
	else
		throw runtime_error("invalid command");

}

void ttrapi::GameServer::writeMsg(ClientInfo& _sock, std::ostringstream& _msg)
{
	auto str = _msg.str();

	if (m_NetLog)
		m_NetLog << '>' << hex << setw(4) << _sock.name << ": " << str << flush;

	if (_sock.pipe)
	{
		_sock.pipe->send(Pipe::CLIENT, move(str));
	}
	else
	{
		if (::send(_sock.sock, str.c_str(), (int)str.length(), 0) != str.length())
			throw connection_failure("connection lost: send failed");
	}
}

void ttrapi::GameServer::clientDeTi(size_t _pid, std::vector<std::string>& _cmd)
{
	if (m_TurnState != TS_WAIT)
		throw rule_violation("invalid game state");

	m_TurnTickets = m_State.drawTicketCards();

	if (m_TurnTickets.empty())
		throw rule_violation("no ticket cards available");

	ostringstream msg;
	msg << (5 + m_TurnTickets.size()) << ' ';

	for (auto& t : m_TurnTickets)
		msg << ' ' << g_CityNames[t.city[0]] << ' ' << g_CityNames[t.city[1]] << ' ' << (size_t)t.points;
	msg << endl;
	writeMsg(m_Clients[_pid], msg);

	m_TurnState = TS_DEST_CARD;
}

void ttrapi::GameServer::clientDTDe(size_t _pid, std::vector<std::string>& _cmd)
{
	if (m_TurnState != TS_DEST_CARD)
		throw rule_violation("invalid game state");

	if (m_State.round == 0)
		m_TurnTickets = m_State.player[_pid].ticketCards;

	vector<int> keep;
	for (size_t i = 2; i < _cmd.size(); ++i)
		keep.push_back(atoi(_cmd[i].c_str()));

	sort(keep.begin(), keep.end());
	keep.erase(unique(keep.begin(), keep.end()), keep.end());
	keep.erase(remove_if(keep.begin(), keep.end(), [&](int _i) { return _i < 0 || _i >= (int)m_TurnTickets.size(); }), keep.end());

	if (m_State.round == 0 && keep.size() < m_State.minTicketsKeep || keep.empty())
		throw rule_violation("keep at least one ticket card or two in case of long tickets");

	if (m_State.round == 0)
		m_State.player[_pid].ticketCards.clear();

	for (int i : keep)
		m_State.player[_pid].ticketCards.push_back(m_TurnTickets[i]);

	m_State.move.type = MT_DrawDestinationTickets;
	m_State.move.ticketsDrawn = (uint8_t)keep.size();
	m_TurnState = TS_DONE;
	sendErrMsg(_pid, 0);
}

void ttrapi::GameServer::clientOpTC(size_t _pid, std::vector<std::string>& _cmd)
{
	if (_cmd.size() < 3)
		throw protocol_error("missing spot number");

	size_t spot = atoi(_cmd[2].c_str());
	if (spot >= OPEN_TRAINS)
		throw protocol_error("invalid spot selected");

	if (m_TurnState != TS_WAIT && m_TurnState != TS_FIRST_CARD)
		throw rule_violation("invalid game state");

	TrainCardColor color = m_State.openTrainCards[spot];
	if (color == TCC_NONE)
		throw rule_violation("there is no card in this spot");

	if (color == TCC_LOCO && m_TurnState == TS_FIRST_CARD)
		throw rule_violation("only one loco allowed per turn");

	m_State.drawOpenTrainCard(spot);
	m_State.player[_pid].trainCards[color]++;
	m_TurnState = (color == TCC_LOCO || m_TurnState == TS_FIRST_CARD) ? TS_DONE : TS_FIRST_CARD;

	ostringstream msg;
	msg << 4;
	for (auto c : m_State.openTrainCards)
		msg << ' ' << g_ColorNames[c];
	msg << ' ' << g_ColorNames[color];
	msg << endl;
	writeMsg(m_Clients[_pid], msg);
}

void ttrapi::GameServer::clientCoTC(size_t _pid, std::vector<std::string>& _cmd)
{
	if (m_TurnState != TS_WAIT && m_TurnState != TS_FIRST_CARD)
		throw rule_violation("invalid game state");

	TrainCardColor color = m_State.drawCovertTrainCard(true);
	if (color == TCC_NONE)
		throw rule_violation("there is no card left");

	m_State.player[_pid].trainCards[color]++;
	m_TurnState = (m_TurnState == TS_FIRST_CARD) ? TS_DONE : TS_FIRST_CARD;

	ostringstream msg;
	msg << 4;
	for (auto c : m_State.openTrainCards)
		msg << ' ' << g_ColorNames[c];
	msg << ' ' << g_ColorNames[color];
	msg << endl;
	writeMsg(m_Clients[_pid], msg);
}

void ttrapi::GameServer::clientRoBu(size_t _pid, std::vector<std::string>& _cmd)
{
	if (_cmd.size() < 4)
		throw protocol_error("missing parameter");

	size_t routeId = atoi(_cmd[2].c_str());
	if (routeId < 1 || routeId > g_Routes.back().id)
		throw protocol_error("unknown route id");

	TrainCardColor color = TCC_NONE;
	for (size_t i = 0; i < TCC_NONE; ++i)
		if (_stricmp(_cmd[3].c_str(), g_ColorNames[i]) == 0)
		{
			color = (TrainCardColor)i;
			break;
		}
	if (color == TCC_NONE)
		throw protocol_error("unknown colour name");

	if (m_TurnState != TS_WAIT)
		throw rule_violation("invalid game state");

	Map<> map;
	map.updateOwner(m_State);
	Route& r = map.routes()[routeId - 1];

	if(!r.canBuild(m_State.player[_pid], color, 0))
		throw rule_violation("not allowed");

	if (!r.def->tunnle)
	{
		r.build(m_State.player[_pid], color, 0, m_State);
		m_TurnState = TS_DONE;
		sendErrMsg(_pid, 0);
		return;
	}

	m_TurnRouteColor = color;
	m_TurnRouteId = routeId;
	m_TurnRouteExtraCost = 0;
	vector<TrainCardColor> extraTrains;

	for (size_t i = 0; i < TUNNLE_CARDS; ++i)
	{
		TrainCardColor extracard = m_State.drawCovertTrainCard();
		if (color == extracard || extracard == TCC_LOCO)
			m_TurnRouteExtraCost++;
		extraTrains.push_back(extracard);
	}
	for(TrainCardColor extracard : extraTrains)
		m_State.dropTrainCard(extracard);

	if (m_TurnRouteExtraCost == 0)
	{
		r.build(m_State.player[_pid], color, 0, m_State);
		m_TurnState = TS_DONE;
		sendErrMsg(_pid, 0);
		return;
	}

	sendErrMsg(_pid, (int)m_TurnRouteExtraCost);
	m_TurnState = TS_TUNNLE;
}

void ttrapi::GameServer::clientRoDe(size_t _pid, std::vector<std::string>& _cmd)
{
	if (_cmd.size() < 3)
		throw protocol_error("missing parameter");

	if (_cmd[2][0] != '0' && _cmd[2][0] != '1')
		throw protocol_error("invalid bool");

	if (m_TurnState != TS_TUNNLE)
		throw rule_violation("invalid game state");

	bool decision = _cmd[2][0] == '1';
	if (decision)
	{
		Map<> map;
		map.updateOwner(m_State);
		Route& r = map.routes()[m_TurnRouteId - 1];
		r.build(m_State.player[_pid], m_TurnRouteColor, m_TurnRouteExtraCost, m_State);
	}
	m_TurnState = TS_DONE;
	sendErrMsg(_pid, 0);
}

void ttrapi::GameServer::clientStBu(size_t _pid, std::vector<std::string>& _cmd)
{
	if (_cmd.size() < 4)
		throw protocol_error("missing parameter");

	CityName city = CN_NONE;
	for(size_t i = 0; i < CN_NONE; ++i)
		if (_stricmp(_cmd[2].c_str(), g_CityNames[i]) == 0)
		{
			city = (CityName)i;
			break;
		}
	if (city == CN_NONE)
		throw protocol_error("unknown city name");

	TrainCardColor color = TCC_NONE;
	for(size_t i = 0; i < TCC_NONE; ++i)
		if(_stricmp(_cmd[3].c_str(), g_ColorNames[i]) == 0)
		{
			color = (TrainCardColor)i;
			break;
		}
	if (color == TCC_NONE)
		throw protocol_error("unknown colour name");

	if (m_TurnState != TS_WAIT)
		throw rule_violation("invalid game state");

	Map<> map;
	map.updateOwner(m_State);

	City& c = map.cities()[city];
	if (!c.build(m_State.player[_pid], color, m_State))
		throw rule_violation("not allowed");

	m_TurnState = TS_DONE;
	sendErrMsg(_pid, 0);
}

void ttrapi::GameServer::clientDefaultMove(size_t _pid)
{
	if (m_TurnState == TS_WAIT || m_TurnState == TS_FIRST_CARD)
	{
		for (size_t i = m_TurnState == TS_FIRST_CARD ? 1 : 0; i < 2; ++i)
		{
			TrainCardColor color = m_State.drawCovertTrainCard(true);
			for (size_t s = 0; s < OPEN_TRAINS && color == TCC_NONE; ++s)
				color = m_State.drawOpenTrainCard(s);
			if (color != TCC_NONE)
				m_State.player[_pid].trainCards[color]++;
		}
	}
	else if(m_TurnState == TS_DEST_CARD && !m_TurnTickets.empty())
	{
		m_State.move.ticketsDrawn = 1;
		m_State.player[_pid].ticketCards.push_back(m_TurnTickets.front());
	}

	m_TurnState = TS_DONE;
}

std::string ttrapi::GameServer::jsonPlayerData(size_t _pid)
{
	using namespace rapidjson;
	Document d(kArrayType);

	for(size_t pid = 0; pid < m_State.player.size(); ++pid)
	{
		const Player& p = m_State.player[pid];

		Value jhash("-1");
		Value jtrainCards(kObjectType);
		Value jdestCards(kArrayType);

		if (pid == _pid)
		{
			ostringstream hashstr;
			hashstr << hex << setw(3) << setfill('0') << m_Turn;
			m_TurnHash = hashstr.str();
			jhash.SetString(hashstr.str().c_str(), d.GetAllocator());

			for(size_t i = 0; i < TCC_NONE; ++i)
				jtrainCards.AddMember(Value(g_ColorNames[i], d.GetAllocator()), Value(p.trainCards[i]), d.GetAllocator());

			for (const auto& ticket : p.ticketCards)
			{
				Value jt(kObjectType);
				jt.AddMember("name", "ticket", d.GetAllocator());
				jt.AddMember("destination1", ticket.city[0], d.GetAllocator());
				jt.AddMember("destination2", ticket.city[1], d.GetAllocator());
				jt.AddMember("points", ticket.points, d.GetAllocator());
				jdestCards.PushBack(jt.Move(), d.GetAllocator());
			}
		}

		Value jp(kObjectType);
		jp.AddMember("id", (uint32_t)p.id, d.GetAllocator());
		jp.AddMember("hash", jhash, d.GetAllocator());
		jp.AddMember("trainCardsDictionary", jtrainCards, d.GetAllocator());
		jp.AddMember("destinationTickets", jdestCards, d.GetAllocator());
		jp.AddMember("stationCount", p.numStationTokens, d.GetAllocator());
		jp.AddMember("trainPieceCount", p.numTrainTokens, d.GetAllocator());
		jp.AddMember("scorePoints", Value(0), d.GetAllocator());
		jp.AddMember("totalTrainCardCount", p.numTrainCards, d.GetAllocator());
		jp.AddMember("totalDestinationTicketCount", p.numTicketCards, d.GetAllocator());
		d.PushBack(jp.Move(), d.GetAllocator());
	}

	StringBuffer buffer;
	Writer<StringBuffer> writer(buffer);
	d.Accept(writer);

	return std::string(buffer.GetString(), buffer.GetLength());
}

std::string ttrapi::GameServer::jsonGameData()
{
	using namespace rapidjson;
	Document d(kObjectType);

	d.AddMember("playerCount", (uint32_t)m_State.player.size(), d.GetAllocator());
	d.AddMember("timeout", m_State.timeout, d.GetAllocator());
	d.AddMember("trainCardCount", m_State.availableTrainCards, d.GetAllocator());
	d.AddMember("destinationTicketCount", m_State.availableTicketCards, d.GetAllocator());
	d.AddMember("seed", m_State.aiSeed, d.GetAllocator());
	
	Value jotc(kArrayType);
	for (TrainCardColor card : m_State.openTrainCards)
		jotc.PushBack(card, d.GetAllocator());
	d.AddMember("openTrainCardSpots", jotc.Move(), d.GetAllocator());

	Value jdtc(kArrayType);
	for (uint32_t i = 0; i < TCC_NONE; ++i)
		for (size_t j = 0; j < m_State.droppedTrainCards[i]; ++j)
			jdtc.PushBack(i, d.GetAllocator());
	d.AddMember("droppedTrainCards", jdtc.Move(), d.GetAllocator());

	Value jro(kObjectType);
	for(size_t i = 0; i < m_State.routeOwner.size(); ++i)
		if (m_State.routeOwner[i] != NO_OWNER)
		{
			ostringstream name;
			name << (size_t)g_Routes[i].id;
			jro.AddMember(Value(name.str().c_str(), d.GetAllocator()), Value(m_State.routeOwner[i]), d.GetAllocator());

		}
	d.AddMember("builtRoutes", jro.Move(), d.GetAllocator());

	Value jso(kObjectType);
	for (size_t i = 0; i < m_State.cityOwner.size(); ++i)
		if (m_State.cityOwner[i] != NO_OWNER)
			jso.AddMember(StringRef(g_CityNames[i]), m_State.cityOwner[i], d.GetAllocator());
	d.AddMember("builtStations", jso.Move(), d.GetAllocator());

	Value jlog(kArrayType);
	for (size_t i = m_State.log.size() < 10 ? 0 : m_State.log.size() - 10; i < m_State.log.size(); i++)
	{
		const Move& move = m_State.log[i];
		Value jmove(kObjectType);
		jmove.AddMember("round", (uint32_t)move.round, d.GetAllocator());
		jmove.AddMember("playerID", (uint32_t)move.playerId, d.GetAllocator());
		jmove.AddMember("turn", move.type, d.GetAllocator());
		jmove.AddMember("defaultTurn", false, d.GetAllocator());

		Value jroute(kNullType);
		if (move.build.route)
		{
			jroute.SetObject();
			jroute.AddMember("id", move.build.route->id, d.GetAllocator());
			jroute.AddMember("length", move.build.route->length, d.GetAllocator());
			jroute.AddMember("type", move.build.route->tunnle ? 3 : move.build.route->locos, d.GetAllocator());
			jroute.AddMember("city1", move.build.route->city[0], d.GetAllocator());
			jroute.AddMember("city2", move.build.route->city[1], d.GetAllocator());
			jroute.AddMember("color", move.build.route->color, d.GetAllocator());
			jroute.AddMember("corouteId", move.build.route->twinId, d.GetAllocator());
		}

		// TODO calculate additional cards in case of tunnle
		jmove.AddMember("additional1Cards", 0, d.GetAllocator());
		jmove.AddMember("additional2Cards", 0, d.GetAllocator());

		if (move.type == MT_BuildRoute || move.type == MT_BuildStation)
		{
			jmove.AddMember("trainCardColor1", move.build.color, d.GetAllocator());
			jmove.AddMember("trainCardColor2", TCC_LOCO, d.GetAllocator());
			jmove.AddMember("card1Count", move.build.numTrainCards, d.GetAllocator());
			jmove.AddMember("card2Count", move.build.numLocoCards, d.GetAllocator());
		}
		else if (!move.trainsDrawn.empty())
		{
			if (move.trainsDrawn.front() == move.trainsDrawn.back())
			{
				jmove.AddMember("trainCardColor1", move.trainsDrawn.front(), d.GetAllocator());
				jmove.AddMember("trainCardColor2", TCC_NONE, d.GetAllocator());
				jmove.AddMember("card1Count", (uint32_t)move.trainsDrawn.size(), d.GetAllocator());
				jmove.AddMember("card2Count", 0, d.GetAllocator());
			}
			else
			{
				jmove.AddMember("trainCardColor1", move.trainsDrawn.front(), d.GetAllocator());
				jmove.AddMember("trainCardColor2", move.trainsDrawn.back(), d.GetAllocator());
				jmove.AddMember("card1Count", 1, d.GetAllocator());
				jmove.AddMember("card2Count", 1, d.GetAllocator());
			}
		}
		else
		{
			jmove.AddMember("trainCardColor1", TCC_NONE, d.GetAllocator());
			jmove.AddMember("trainCardColor2", TCC_NONE, d.GetAllocator());
			jmove.AddMember("card1Count", move.ticketsDrawn, d.GetAllocator());
			jmove.AddMember("card2Count", move.ticketsDrawn, d.GetAllocator());
		}

		jmove.AddMember("city", move.build.station, d.GetAllocator());
		jmove.AddMember("route", jroute, d.GetAllocator());
		jmove.AddMember("success", move.build.success, d.GetAllocator());		
		jlog.PushBack(jmove.Move(), d.GetAllocator());
	}
	d.AddMember("log", jlog.Move(), d.GetAllocator());

	StringBuffer buffer;
	Writer<StringBuffer> writer(buffer);
	d.Accept(writer);

	return std::string(buffer.GetString(), buffer.GetLength());
}

template<typename T, typename Tr>
void shuffle_container(Tr& _rand, T& _in, T& _out)
{
	sort(_in.begin(), _in.end());
	while (!_in.empty())
	{
		auto it = _in.begin() + _rand() % _in.size();
		_out.push_back(*it);
		*it = _in.back();
		_in.pop_back();
	}
}

void ttrapi::ServerGameState::shuffle(uint32_t _seed, bool _longRoutes)
{
	minTicketsKeep = _longRoutes ? 2 : 1;
	round = 0;
	rand.seed(_seed);
	aiSeed = rand();

	vector<TicketDef> ticketsLong, ticketsLongStack;
	vector<TicketDef> tickets;
	for (auto t : g_Tickets)
	{
		sort(t.city, t.city + 2);
		(t.points < 20 ? tickets : ticketsLong).push_back(t);
	}
	shuffle_container(rand, tickets, ticketStack);
	reverse(ticketStack.begin(), ticketStack.end());

	if (_longRoutes)
	{
		shuffle_container(rand, ticketsLong, ticketsLongStack);
		reverse(ticketsLongStack.begin(), ticketsLongStack.end());
	}

	for (Player& p : player)
	{
		auto tickets = drawTicketCards();
		p.ticketCards.insert(p.ticketCards.end(), tickets.begin(), tickets.end());

		if (_longRoutes)
		{
			p.ticketCards.push_back(ticketsLongStack.back());
			ticketsLongStack.pop_back();
		}

		for (uint8_t& i : p.trainCards)
			i = 0;

		p.numStationTokens = TOKEN_STATION;
		p.numTrainTokens = TOKEN_TRAIN;
	}

	droppedTrainStack.insert(droppedTrainStack.end(), CARDS_LOCOMOTIVE, TCC_LOCO);
	for (size_t i = 1; i < TCC_NONE; ++i)
		droppedTrainStack.insert(droppedTrainStack.end(), CARDS_PER_COLOR, (TrainCardColor)i);

	for (size_t i = 0; i < INITIAL_TRAIN_CARDS_PER_PLAYER; ++i)
		for (Player& p : player)
			p.trainCards[drawCovertTrainCard()]++;

	for (auto& spot : openTrainCards)
		spot = drawCovertTrainCard();
	checkOpenTrainCardsForLocos();
	updateCountVars();
}

void ttrapi::ServerGameState::updateCountVars()
{
	availableTrainCards = (uint8_t)(droppedTrainStack.size() + trainStack.size());

	for (auto& openCard : openTrainCards)
		if (openCard != TCC_NONE)
			availableTrainCards++;

	availableTicketCards = (uint8_t)ticketStack.size();

	for (Player& p : player)
	{
		p.numTicketCards = (uint8_t)p.ticketCards.size();
		p.numTrainCards = 0;
		for (uint8_t c : p.trainCards)
			p.numTrainCards += c;
	}
}

void ttrapi::ServerGameState::checkOpenTrainCardsForLocos()
{
	size_t tries = 3;
	while (tries-- && count(openTrainCards.begin(), openTrainCards.end(), TCC_LOCO) >= OPEN_LOCO_LIMIT)
	{
		for (auto color : openTrainCards)
			dropTrainCard(color);
		for (auto& color : openTrainCards)
			color = drawCovertTrainCard();
	}
}

ttrapi::TrainCardColor ttrapi::ServerGameState::drawCovertTrainCard(bool _log)
{
	if (trainStack.empty())
	{
		if (droppedTrainStack.empty())
			return TCC_NONE;
		else
			shuffle_container(rand, droppedTrainStack, trainStack);
	}

	TrainCardColor drawn = trainStack.back();
	trainStack.pop_back();

	if (_log)
	{
		move.type = MT_DrawTrainCards;
		move.trainsDrawn.push_back(TCC_ALL);
	}

	return drawn;
}

ttrapi::TrainCardColor ttrapi::ServerGameState::drawOpenTrainCard(size_t _spot)
{
	TrainCardColor drawn = openTrainCards[_spot];
	if (drawn != TCC_NONE)
	{
		openTrainCards[_spot] = drawCovertTrainCard();
		move.type = MT_DrawTrainCards;
		move.trainsDrawn.push_back(drawn);
		checkOpenTrainCardsForLocos();
	}
	return drawn;
}

vector<ttrapi::TicketDef> ttrapi::ServerGameState::drawTicketCards()
{
	vector<TicketDef> drawn = vector<TicketDef>();
	for (size_t i = 0; i < DRAW_TICKETS_COUNT && !ticketStack.empty(); i++)
	{
		drawn.push_back(ticketStack.back());
		ticketStack.pop_back();
	}

	move.type = MT_DrawDestinationTickets;
	move.ticketsDrawn = (uint8_t)drawn.size();

	return drawn;
}

void ttrapi::ServerGameState::pay(Player & _player, TrainCardColor _color, size_t _cost, size_t _locos)
{
	size_t extraLocos = 0;
	if (_cost - _locos > _player.trainCards[_color])
		extraLocos = _cost - _locos - _player.trainCards[_color];
	size_t locoCost = extraLocos + _locos;
	size_t colorCost = _cost - locoCost;

	_player.trainCards[TCC_LOCO] -= (uint8_t)locoCost;
	_player.trainCards[_color] -= (uint8_t)colorCost;

	move.build.color = _color;
	move.build.numLocoCards = (uint8_t)locoCost;
	move.build.numTrainCards = (uint8_t)colorCost;
	move.build.success = true;

	for (size_t i = 0; i < colorCost; i++)
		dropTrainCard(_color);

	for(size_t i = 0; i < locoCost; i++)
		dropTrainCard(TCC_LOCO);
	
	for (auto& openCard : openTrainCards)
	{
		if (openCard == TCC_NONE)
			openCard = drawCovertTrainCard();
	}
	checkOpenTrainCardsForLocos();
}

void ttrapi::ServerGameState::dropTrainCard(TrainCardColor _color)
{
	if (_color < TCC_NONE)
	{
		droppedTrainStack.push_back(_color);
		droppedTrainCards[_color]++;
	}
}

int ttrapi::GameState::points(const Player & _p, bool _tickets) const
{
	Map<> map;
	map.updateOwner(*this);

	int sum = _p.numStationTokens * TOKEN_STATION_POINTS;

	for (auto& route : map.routes())
		if (route.owner && route.owner->id == _p.id)
			sum += (int)route.points();

	if (!_tickets)
		return sum;

	array<bool, g_Routes.size()> playerRoutes;
	transform(routeOwner.begin(), routeOwner.end(), playerRoutes.begin(), [&](uint8_t _id) { return _id == _p.id; });
	auto costFunc = [&](const CityLink& _l) { return playerRoutes[_l.route.def->id - 1] ? 0 : -1; };

	vector<TicketDef> leftOverTickets;
	for (const TicketDef& ticket : _p.ticketCards)
	{
		auto path = map.dijkstra(ticket.city[0], ticket.city[1], costFunc);
		if (!path.empty())
			sum += ticket.points;
		else
			leftOverTickets.push_back(ticket);
	}
	if (leftOverTickets.empty())
		return sum;

	vector<vector<Route*> > stations;
	for(auto& city : map.cities())
		if (city.owner && city.owner->id == _p.id)
		{
			stations.emplace_back();
			for (auto& link : city.routes)
				if (link.route.owner && link.route.owner->id != _p.id)
					stations.back().push_back(&link.route);
		}

	if (stations.empty())
	{
		int malus = accumulate(leftOverTickets.begin(), leftOverTickets.end(), 0, [](int _a, TicketDef& _b) { return _a - _b.points; });
		return sum + malus;
	}

	int bestSum = numeric_limits<int>::min();
	size_t numOptions = accumulate(stations.begin(), stations.end(), (size_t)1, [](size_t _a, vector<Route*>& _b) { return _a * _b.size(); });
	for (size_t i = 0; i < numOptions; ++i)
	{
		size_t option = i;
		for (auto station : stations)
		{
			playerRoutes[station[option % station.size()]->def->id - 1] = true;
			option /= station.size();
		}

		bool allConnected = true;
		int newSum = sum;
		for (auto& ticket : leftOverTickets)
		{
			if (!map.dijkstra(ticket.city[0], ticket.city[1], costFunc).empty())
			{
				newSum += ticket.points;
			}
			else
			{
				newSum -= ticket.points;
				allConnected = false;
			}
		}
		bestSum = max(bestSum, newSum);
		if (allConnected)
			break;

		option = i;
		for (auto station : stations)
		{
			playerRoutes[station[option % station.size()]->def->id - 1] = false;
			option /= station.size();
		}
	}
	return bestSum;
}

int ttrapi::GameState::longestPathPlayer() const
{
	Map<> map;
	map.updateOwner(*this);

	size_t maxLen = 0;
	int maxPid = -1;

	for (auto& p : player)
	{
		for (auto& city : map.cities())
		{
			auto path = map.longestPath(city.name, p);

			size_t len = 0;
			for (CityLink* it : path)
				len += it->route.def->length;
			if (len > maxLen)
			{
				maxLen = len;
				maxPid = (int)p.id;
			}
		}
	}

	return maxPid;
}

void ttrapi::Pipe::send(Name _to, std::string _str)
{
	{
		lock_guard<mutex> lock(mut);
		buf[_to].push(move(_str));
	}
	cond.notify_one();
}

bool ttrapi::Pipe::recv(Name _from, std::string & _str)
{
	unique_lock<mutex> lock(mut);
	
	while (buf[_from].empty())
		cond.wait(lock);

	_str = buf[_from].front();
	buf[_from].pop();
	return true;
}

ttrapi::SocketWrapper::SocketWrapper()
{
	initNetwork();
	socket_ = (unsigned int)::socket(AF_INET, SOCK_STREAM, 0);
	if (socket_ == ~0u)
		throw runtime_error("failed to create inet socket");
}

ttrapi::SocketWrapper::~SocketWrapper()
{
	if (socket_ != ~0u)
		closesocket(socket_);
}

ttrapi::SocketWrapper::SocketWrapper(SocketWrapper && _o)
{
	socket_ = _o.socket_;
	_o.socket_ = ~0u;
}

bool ttrapi::SocketWrapper::listen(uint16_t _port)
{
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(_port);
	addr.sin_addr.s_addr = ADDR_ANY;
	if (::bind(socket_, (sockaddr*)&addr, sizeof(addr)) != 0)
		return false;

	if (::listen(socket_, 5) != 0)
		return false;

	return true;
}
