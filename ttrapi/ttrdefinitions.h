#pragma once
#include <stdint.h>
#include <array>
#include <vector>
#include <random>

namespace ttrapi
{
	enum {
		OPEN_TRAINS = 5,
		OPEN_LOCO_LIMIT = 3,
		TOKEN_STATION = 3,
		TOKEN_STATION_POINTS = 4,
		TOKEN_TRAIN = 45,
		TOKEN_TRAIN_MIN = 2,
		TUNNLE_CARDS = 3,
		DRAW_TICKETS_COUNT = 3,
		CARDS_PER_COLOR = 12,
		CARDS_LOCOMOTIVE = 14,
		INITIAL_TRAIN_CARDS_PER_PLAYER = 4,
		POINTS_FOR_LONGEST_PATH = 10,
		NO_OWNER = 0xff,
	};

	static const uint8_t g_RoutePoints[] = {
		0, 1, 2, 4, 7, 7, 15, 15, 21
	};

	enum TrainCardColor
	{
		TCC_LOCO = 0,
		TCC_BLUE = 1,
		TCC_YELLOW = 2,
		TCC_GREEN = 3,
		TCC_ORANGE = 4,
		TCC_PINK = 5,
		TCC_RED = 6,
		TCC_BLACK = 7,
		TCC_WHITE = 8,
		TCC_NONE = 9,
		TCC_ALL = 10
	};

	static const char* const g_ColorNames[] = {
		"loco",
		"blue",
		"yellow",
		"green",
		"orange",
		"pink",
		"red",
		"black",
		"white",
		"none",
		"all"
	};

	enum CityName
	{
		CN_AMSTERDAM = 0,
		CN_ANGORA = 1,
		CN_ATHINA = 2,
		CN_BARCELONA = 3,
		CN_BERLIN = 4,
		CN_BREST = 5,
		CN_BRINDISI = 6,
		CN_BRUXELLES = 7,
		CN_BUCURESTI = 8,
		CN_BUDAPEST = 9,
		CN_CADIZ = 10,
		CN_CONSTANTINOPLE = 11,
		CN_DANZIG = 12,
		CN_DIEPPE = 13,
		CN_EDINBURGH = 14,
		CN_ERZURUM = 15,
		CN_ESSEN = 16,
		CN_FRANKFURT = 17,
		CN_KHARKOV = 18,
		CN_KOBENHAVN = 19,
		CN_KYIV = 20,
		CN_LISBOA = 21,
		CN_LONDON = 22,
		CN_MADRID = 23,
		CN_MARSEILLE = 24,
		CN_MOSKVA = 25,
		CN_MUNCHEN = 26,
		CN_PALERMO = 27,
		CN_PAMPLONA = 28,
		CN_PARIS = 29,
		CN_PETROGRAD = 30,
		CN_RIGA,
		CN_ROMA,
		CN_ROSTOV,
		CN_SARAJEVO,
		CN_SEVASTOPOL,
		CN_SMOLENSK,
		CN_SMYRNA,
		CN_SOCHI,
		CN_SOFIA,
		CN_STOCKHOLM,
		CN_VENEZIA,
		CN_WARSZAWA,
		CN_WIEN,
		CN_WILNO,
		CN_ZAGRAB,
		CN_ZURICH,
		CN_NONE
	};

	static const char* const g_CityNames[] = {
		"Amsterdam",
		"Angora",
		"Athina",
		"Barcelona",
		"Berlin",
		"Brest",
		"Brindisi",
		"Bruxelles",
		"Bucuresti",
		"Budapest",
		"Cadiz",
		"Constantinople",
		"Danzig",
		"Dieppe",
		"Edinburgh",
		"Erzurum",
		"Essen",
		"Frankfurt",
		"Kharkov",
		"Kobenhavn",
		"Kyiv",
		"Lisboa",
		"London",
		"Madrid",
		"Marseille",
		"Moskva",
		"Munchen",
		"Palermo",
		"Pamplona",
		"Paris",
		"Petrograd",
		"Riga",
		"Roma",
		"Rostov",
		"Sarajevo",
		"Sevastopol",
		"Smolensk",
		"Smyrna",
		"Sochi",
		"Sofia",
		"Stockholm",
		"Venezia",
		"Warszawa",
		"Wien",
		"Wilno",
		"Zagrab",
		"Zurich",
		"None",
	};

	struct TicketDef
	{
		CityName	city[2];
		uint8_t		points;

		bool operator < (const TicketDef& _o) const {
			if (points != _o.points)
				return points < _o.points;
			if(city[0] != _o.city[0])
				return city[0] < _o.city[0];
			return city[1] < _o.city[1];
		}
	};

	struct RouteDef
	{
		uint8_t			id;
		uint8_t			twinId;
		uint8_t			length;
		uint8_t			locos;
		bool			tunnle;
		CityName		city[2];
		TrainCardColor	color;
	};

	// note: id and twinId equals index + 1
	constexpr static const std::array<RouteDef, 101> g_Routes = { {
		{ 1,0,2,0,false,{ CN_BERLIN,CN_ESSEN },TCC_BLUE },
		{ 2,0,4,0,true,{ CN_BUDAPEST,CN_BUCURESTI },TCC_ALL },
		{ 3,4,4,0,false,{ CN_EDINBURGH,CN_LONDON },TCC_BLACK },
		{ 4,3,4,0,false,{ CN_EDINBURGH,CN_LONDON },TCC_ORANGE },
		{ 5,6,2,1,false,{ CN_LONDON,CN_DIEPPE },TCC_ALL },
		{ 6,5,2,1,false,{ CN_LONDON,CN_DIEPPE },TCC_ALL },
		{ 7,0,2,2,false,{ CN_LONDON,CN_AMSTERDAM },TCC_ALL },
		{ 8,0,2,0,false,{ CN_BREST,CN_DIEPPE },TCC_ORANGE },
		{ 9,0,3,0,false,{ CN_BREST,CN_PARIS },TCC_BLACK },
		{ 10,0,4,0,false,{ CN_BREST,CN_PAMPLONA },TCC_PINK },
		{ 11,0,1,0,false,{ CN_DIEPPE,CN_PARIS },TCC_PINK },
		{ 12,0,2,0,false,{ CN_DIEPPE,CN_BRUXELLES },TCC_GREEN },
		{ 13,0,3,0,false,{ CN_AMSTERDAM,CN_ESSEN },TCC_YELLOW },
		{ 14,0,1,0,false,{ CN_AMSTERDAM,CN_BRUXELLES },TCC_BLACK },
		{ 15,16,2,0,false,{ CN_PARIS,CN_BRUXELLES },TCC_YELLOW },
		{ 16,15,2,0,false,{ CN_PARIS,CN_BRUXELLES },TCC_RED },
		{ 17,18,3,0,false,{ CN_PARIS,CN_FRANKFURT },TCC_WHITE },
		{ 18,17,3,0,false,{ CN_PARIS,CN_FRANKFURT },TCC_ORANGE },
		{ 19,20,4,0,false,{ CN_PARIS,CN_PAMPLONA },TCC_BLUE },
		{ 20,19,4,0,false,{ CN_PARIS,CN_PAMPLONA },TCC_GREEN },
		{ 21,0,4,0,false,{ CN_PARIS,CN_MARSEILLE },TCC_ALL },
		{ 22,0,3,0,true,{ CN_PARIS,CN_ZURICH },TCC_ALL },
		{ 23,0,2,0,false,{ CN_BRUXELLES,CN_FRANKFURT },TCC_BLUE },
		{ 24,0,2,0,false,{ CN_AMSTERDAM,CN_FRANKFURT },TCC_WHITE },
		{ 25,0,4,0,false,{ CN_PAMPLONA,CN_MARSEILLE },TCC_RED },
		{ 26,27,3,0,true,{ CN_MADRID,CN_PAMPLONA },TCC_BLACK },
		{ 27,26,3,0,true,{ CN_MADRID,CN_PAMPLONA },TCC_WHITE },
		{ 28,0,2,0,false,{ CN_MADRID,CN_BARCELONA },TCC_YELLOW },
		{ 29,0,2,0,true,{ CN_BARCELONA,CN_PAMPLONA },TCC_ALL },
		{ 30,0,4,0,false,{ CN_BARCELONA,CN_MARSEILLE },TCC_ALL },
		{ 31,0,3,0,false,{ CN_LISBOA,CN_MADRID },TCC_PINK },
		{ 32,0,2,0,false,{ CN_LISBOA,CN_CADIZ },TCC_BLUE },
		{ 33,0,3,0,false,{ CN_CADIZ,CN_MADRID },TCC_ORANGE },
		{ 34,0,2,0,true,{ CN_MARSEILLE,CN_ZURICH },TCC_PINK },
		{ 35,0,4,0,true,{ CN_MARSEILLE,CN_ROMA },TCC_ALL },
		{ 36,0,2,0,true,{ CN_ZURICH,CN_MUNCHEN },TCC_YELLOW },
		{ 37,0,2,0,true,{ CN_ZURICH,CN_VENEZIA },TCC_GREEN },
		{ 38,0,2,0,false,{ CN_VENEZIA,CN_ZAGRAB },TCC_ALL },
		{ 39,0,2,0,false,{ CN_VENEZIA,CN_ROMA },TCC_BLACK },
		{ 40,0,3,0,false,{ CN_MUNCHEN,CN_WIEN },TCC_ORANGE },
		{ 41,0,2,0,true,{ CN_MUNCHEN,CN_VENEZIA },TCC_BLUE },
		{ 42,0,2,0,false,{ CN_FRANKFURT,CN_ESSEN },TCC_GREEN },
		{ 43,44,3,0,false,{ CN_FRANKFURT,CN_BERLIN },TCC_BLACK },
		{ 44,43,3,0,false,{ CN_FRANKFURT,CN_BERLIN },TCC_RED },
		{ 45,0,2,0,false,{ CN_FRANKFURT,CN_MUNCHEN },TCC_PINK },
		{ 46,47,3,1,false,{ CN_ESSEN,CN_KOBENHAVN },TCC_ALL },
		{ 47,46,3,1,false,{ CN_ESSEN,CN_KOBENHAVN },TCC_ALL },
		{ 48,49,3,0,false,{ CN_KOBENHAVN,CN_STOCKHOLM },TCC_YELLOW },
		{ 49,48,3,0,false,{ CN_KOBENHAVN,CN_STOCKHOLM },TCC_WHITE },
		{ 50,0,8,0,true,{ CN_STOCKHOLM,CN_PETROGRAD },TCC_ALL },
		{ 51,0,4,0,false,{ CN_BERLIN,CN_DANZIG },TCC_ALL },
		{ 52,53,4,0,false,{ CN_BERLIN,CN_WARSZAWA },TCC_PINK },
		{ 53,52,4,0,false,{ CN_BERLIN,CN_WARSZAWA },TCC_YELLOW },
		{ 54,0,3,0,false,{ CN_BERLIN,CN_WIEN },TCC_GREEN },
		{ 55,0,4,0,false,{ CN_WIEN,CN_WARSZAWA },TCC_BLUE },
		{ 56,57,1,0,false,{ CN_WIEN,CN_BUDAPEST },TCC_RED },
		{ 57,56,1,0,false,{ CN_WIEN,CN_BUDAPEST },TCC_WHITE },
		{ 58,0,2,0,false,{ CN_WIEN,CN_ZAGRAB },TCC_ALL },
		{ 59,0,3,0,false,{ CN_ZAGRAB,CN_SARAJEVO },TCC_RED },
		{ 60,0,6,0,true,{ CN_BUDAPEST,CN_KYIV },TCC_ALL },
		{ 61,0,2,0,false,{ CN_ZAGRAB,CN_BUDAPEST },TCC_ORANGE },
		{ 62,0,3,0,false,{ CN_BUDAPEST,CN_SARAJEVO },TCC_PINK },
		{ 63,0,3,0,false,{ CN_DANZIG,CN_RIGA },TCC_BLACK },
		{ 64,0,2,0,false,{ CN_DANZIG,CN_WARSZAWA },TCC_ALL },
		{ 65,0,3,0,false,{ CN_WARSZAWA,CN_WILNO },TCC_RED },
		{ 66,0,4,0,false,{ CN_WARSZAWA,CN_KYIV },TCC_ALL },
		{ 67,0,4,0,false,{ CN_RIGA,CN_PETROGRAD },TCC_ALL },
		{ 68,0,4,0,false,{ CN_RIGA,CN_WILNO },TCC_GREEN },
		{ 69,0,4,0,false,{ CN_WILNO,CN_PETROGRAD },TCC_BLUE },
		{ 70,0,3,0,false,{ CN_WILNO,CN_SMOLENSK },TCC_YELLOW },
		{ 71,0,2,0,false,{ CN_WILNO,CN_KYIV },TCC_ALL },
		{ 72,0,4,0,false,{ CN_PETROGRAD,CN_MOSKVA },TCC_WHITE },
		{ 73,0,2,0,false,{ CN_SMOLENSK,CN_MOSKVA },TCC_ORANGE },
		{ 74,0,4,0,false,{ CN_MOSKVA,CN_KHARKOV },TCC_ALL },
		{ 75,0,3,0,false,{ CN_KYIV,CN_SMOLENSK },TCC_RED },
		{ 76,0,4,0,false,{ CN_KYIV,CN_KHARKOV },TCC_ALL },
		{ 77,0,2,0,false,{ CN_KHARKOV,CN_ROSTOV },TCC_GREEN },
		{ 78,0,2,0,false,{ CN_ROMA,CN_BRINDISI },TCC_WHITE },
		{ 79,0,4,1,false,{ CN_ROMA,CN_PALERMO },TCC_ALL },
		{ 80,0,3,1,false,{ CN_PALERMO,CN_BRINDISI },TCC_ALL },
		{ 81,0,6,2,false,{ CN_PALERMO,CN_SMYRNA },TCC_ALL },
		{ 82,0,4,1,false,{ CN_BRINDISI,CN_ATHINA },TCC_ALL },
		{ 83,0,2,0,true,{ CN_SARAJEVO,CN_SOFIA },TCC_ALL },
		{ 84,0,4,0,false,{ CN_SARAJEVO,CN_ATHINA },TCC_GREEN },
		{ 85,0,2,0,true,{ CN_SOFIA,CN_BUCURESTI },TCC_ALL },
		{ 86,0,3,0,false,{ CN_SOFIA,CN_CONSTANTINOPLE },TCC_BLUE },
		{ 87,0,3,0,false,{ CN_ATHINA,CN_SOFIA },TCC_PINK },
		{ 88,0,2,1,false,{ CN_ATHINA,CN_SMYRNA },TCC_ALL },
		{ 89,0,4,0,false,{ CN_BUCURESTI,CN_KYIV },TCC_ALL },
		{ 90,0,4,0,false,{ CN_BUCURESTI,CN_SEVASTOPOL },TCC_WHITE },
		{ 91,0,3,0,false,{ CN_BUCURESTI,CN_CONSTANTINOPLE },TCC_YELLOW },
		{ 92,0,4,0,false,{ CN_SEVASTOPOL,CN_ROSTOV },TCC_ALL },
		{ 93,0,2,1,false,{ CN_SEVASTOPOL,CN_SOCHI },TCC_ALL },
		{ 94,0,2,0,false,{ CN_ROSTOV,CN_SOCHI },TCC_ALL },
		{ 95,0,2,0,true,{ CN_SMYRNA,CN_CONSTANTINOPLE },TCC_ALL },
		{ 96,0,3,0,true,{ CN_SMYRNA,CN_ANGORA },TCC_ORANGE },
		{ 97,0,4,2,false,{ CN_CONSTANTINOPLE,CN_SEVASTOPOL },TCC_ALL },
		{ 98,0,2,0,true,{ CN_CONSTANTINOPLE,CN_ANGORA },TCC_ALL },
		{ 99,0,3,0,false,{ CN_ANGORA,CN_ERZURUM },TCC_BLACK },
		{ 100,0,4,2,false,{ CN_SEVASTOPOL,CN_ERZURUM },TCC_ALL },
		{ 101,0,3,0,true,{ CN_ERZURUM,CN_SOCHI },TCC_RED }
	} };

	constexpr static const std::array<TicketDef, 46> g_Tickets = { {
		{ { CN_ATHINA, CN_ANGORA }, 5 },
		{ { CN_BUDAPEST, CN_SOFIA }, 5 },
		{ { CN_FRANKFURT, CN_KOBENHAVN }, 5 },
		{ { CN_ROSTOV, CN_ERZURUM }, 5 },
		{ { CN_SOFIA, CN_SMYRNA }, 5 },
		{ { CN_KYIV, CN_PETROGRAD }, 6 },
		{ { CN_ZURICH, CN_BRINDISI }, 6 },
		{ { CN_ZURICH, CN_BUDAPEST }, 6 },
		{ { CN_WARSZAWA, CN_SMOLENSK }, 6 },
		{ { CN_ZAGRAB, CN_BRINDISI }, 6 },
		{ { CN_PARIS, CN_ZAGRAB }, 7 },
		{ { CN_BREST, CN_MARSEILLE }, 7 },
		{ { CN_LONDON, CN_BERLIN }, 7 },
		{ { CN_EDINBURGH, CN_PARIS }, 7 },
		{ { CN_AMSTERDAM, CN_PAMPLONA }, 7 },
		{ { CN_ROMA, CN_SMYRNA }, 8 },
		{ { CN_PALERMO, CN_CONSTANTINOPLE }, 8 },
		{ { CN_SARAJEVO, CN_SEVASTOPOL }, 8 },
		{ { CN_MADRID, CN_DIEPPE }, 8 },
		{ { CN_BARCELONA, CN_BRUXELLES }, 8 },
		{ { CN_PARIS, CN_WIEN }, 8 },
		{ { CN_BARCELONA, CN_MUNCHEN }, 8 },
		{ { CN_BREST, CN_VENEZIA }, 8 },
		{ { CN_SMOLENSK, CN_ROSTOV }, 8 },
		{ { CN_MARSEILLE, CN_ESSEN }, 8 },
		{ { CN_KYIV, CN_SOCHI }, 8 },
		{ { CN_MADRID, CN_ZURICH }, 8 },
		{ { CN_BERLIN, CN_BUCURESTI }, 8 },
		{ { CN_BRUXELLES, CN_DANZIG }, 9 },
		{ { CN_BERLIN, CN_ROMA }, 9 },
		{ { CN_ANGORA, CN_KHARKOV }, 10 },
		{ { CN_RIGA, CN_BUCURESTI }, 10 },
		{ { CN_ESSEN, CN_KYIV }, 10 },
		{ { CN_VENEZIA, CN_CONSTANTINOPLE }, 10 },
		{ { CN_LONDON, CN_WIEN }, 10 },
		{ { CN_ATHINA, CN_WILNO }, 11 },
		{ { CN_STOCKHOLM, CN_WIEN }, 11 },
		{ { CN_BERLIN, CN_MOSKVA }, 12 },
		{ { CN_AMSTERDAM, CN_WILNO }, 12 },
		{ { CN_FRANKFURT, CN_SMOLENSK }, 13 },
		{ { CN_LISBOA, CN_DANZIG }, 20 },
		{ { CN_BREST, CN_PETROGRAD }, 20 },
		{ { CN_PALERMO, CN_MOSKVA }, 20 },
		{ { CN_KOBENHAVN, CN_ERZURUM }, 21 },
		{ { CN_EDINBURGH, CN_ATHINA }, 21 },
		{ { CN_CADIZ, CN_STOCKHOLM }, 21 }
	} };

	enum {
		NUM_ROUTES = g_Routes.size(),
		NUM_TICKETS = g_Tickets.size(),
	};


	enum MoveType
	{
		MT_DrawTrainCards,
		MT_DrawDestinationTickets,
		MT_BuildRoute,
		MT_BuildStation,
		MT_NoTurnWasPossible
	};

	struct Move
	{
		MoveType		type = MT_NoTurnWasPossible;
		size_t			round = 0;
		size_t			playerId = 0;

		struct {
			TrainCardColor	color = TCC_NONE;
			CityName		station = CN_NONE;
			const RouteDef*	route = nullptr;
			uint8_t			numTrainCards = 0;
			uint8_t			numLocoCards = 0;
			bool			success = false;
		} build;

		std::vector<TrainCardColor> trainsDrawn;
		uint8_t						ticketsDrawn;
	};

	struct Player
	{
		size_t					id;
		uint8_t					trainCards[TCC_ALL + 1];
		std::vector<TicketDef>	ticketCards;

		uint8_t	numStationTokens;
		uint8_t numTrainTokens;
		uint8_t	numTrainCards;
		uint8_t	numTicketCards;
	};

	enum {
		OWNER_NONE = 5
	};

	struct GameState
	{
		GameState()
		{
			openTrainCards.fill(TCC_NONE);
			droppedTrainCards.fill(0);
			routeOwner.fill(NO_OWNER);
			cityOwner.fill(NO_OWNER);
		}

		int points(const Player& _p, bool _tickets = true) const;
		int longestPathPlayer() const;

		std::vector<Player>		player;
		float					timeout = 0;
		uint16_t				round = 0xFFFF;
		uint8_t					availableTrainCards = 0;
		uint8_t					availableTicketCards = 0;
		uint32_t				aiSeed;

		std::array<TrainCardColor, 5>			openTrainCards;
		std::array<uint8_t, TCC_ALL>			droppedTrainCards;
		std::array<uint8_t, g_Routes.size()>	routeOwner;
		std::array<uint8_t, CN_NONE>			cityOwner;

		std::vector<Move>		log;
	};

	struct ServerGameState : GameState
	{
		void shuffle(uint32_t _seed, bool _longRoutes = false);
		TrainCardColor drawCovertTrainCard(bool _log = false);
		TrainCardColor drawOpenTrainCard(size_t _spot);
		std::vector<TicketDef> drawTicketCards();
		void pay(Player& _player, TrainCardColor _color, size_t _cost, size_t _locos = 0);
		void dropTrainCard(TrainCardColor _color);
		void updateCountVars();
		void checkOpenTrainCardsForLocos();

		std::mt19937					rand;
		std::vector<TicketDef>			ticketStack;
		std::vector<TrainCardColor>		trainStack;
		std::vector<TrainCardColor>		droppedTrainStack;
		Move							move;
		size_t							minTicketsKeep = 0;
	};
}
