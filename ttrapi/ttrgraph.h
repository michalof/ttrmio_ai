#pragma once
#include "ttrdefinitions.h"

#include <functional>
#include <vector>
#include <algorithm>

namespace ttrapi
{
	class MapBase;
	struct City;
	struct Route;

	struct CityLink
	{
		City&	dest;
		Route&	route;
	};

	struct City
	{
		MapBase*				map;
		CityName				name;
		const Player*			owner = nullptr;
		std::vector<CityLink>	routes;

		CityLink* link(CityName _dest);
		bool available() const { return owner == nullptr; }
		bool canBuild(const Player& _player) const;
		bool canBuild(const Player& _player, TrainCardColor _color) const;
		bool build(Player& _player, TrainCardColor _color, ServerGameState& _state);
	};

	struct Route
	{
		MapBase*				map;
		const RouteDef*			def;
		const Player*			owner = nullptr;
		City*					city[2];
		Route*					twin = nullptr;

		bool available() const;
		bool build(Player& _player, TrainCardColor _color, size_t _extraCost, ServerGameState& _state);
		TrainCardColor canBuild(const Player& _player, size_t _extraCost = 0) const;
		bool canBuild(const Player& _player, TrainCardColor _color, size_t _extraCost) const;
		size_t points() const { return g_RoutePoints[def->length]; }

		bool passable(const Player& _player) const;
	};

	class MapBase
	{
	public:
		bool allowDoubleTracks() const { return m_AllowDoubleTracks; }

	protected:
		bool								m_AllowDoubleTracks = true;
	};

	template<typename Tr = Route, typename Tc = City, typename Tl = CityLink>
	class Map : public MapBase
	{
	public:
		Map();

		void updateOwner(const GameState& _conn);

		auto& cities() { return m_Cities; }
		auto& cities() const { return m_Cities; }
		auto& routes() { return m_Routes; }
		auto& routes() const { return m_Routes; }


		std::vector<Tl*> dijkstra(CityName _start, CityName _end,
			std::function<int(const Tl&)> _cost = [](const Tl& _l) {return _l.route.def->length; })
		{
			return dijkstra(_start, [=](const Tl& _l) {return _l.dest.name == _end; }, _cost);
		}

		std::vector<Tl*> dijkstra(CityName _start,
			std::function<bool (const Tl&)> _end,
			std::function<int (const Tl&)> _cost);

		std::vector<Tl*> longestPath(CityName _start, const Player& _player);

	protected:
		std::array<Tc, CN_NONE>		m_Cities;
		std::array<Tr, NUM_ROUTES>	m_Routes;
	};

	template<typename Tr, typename Tc, typename Tl>
	Map<Tr,Tc,Tl>::Map()
	{
		for (size_t i = 0; i < m_Cities.size(); ++i)
		{
			m_Cities[i].map = this;
			m_Cities[i].name = (CityName)i;
		}

		for (size_t i = 0; i < m_Routes.size(); ++i)
		{
			Tr& r = m_Routes[i];
			r.map = this;
			r.def = &g_Routes[i];
			r.city[0] = &m_Cities[r.def->city[0]];
			r.city[1] = &m_Cities[r.def->city[1]];

			if (r.def->twinId)
				r.twin = &m_Routes[r.def->twinId - 1];

			for (size_t c = 0; c < 2; ++c)
				r.city[c]->routes.push_back({ *r.city[c ^ 1], r });
		}
	}

	template<typename Tr, typename Tc, typename Tl>
	void Map<Tr, Tc, Tl>::updateOwner(const GameState& _state)
	{
		m_AllowDoubleTracks = _state.player.size() >= 4;

		for (size_t i = 0; i < m_Cities.size(); ++i)
		{
			uint8_t owner = _state.cityOwner[i];
			if (owner != NO_OWNER)
				m_Cities[i].owner = &_state.player[owner];
		}

		for (size_t i = 0; i < m_Routes.size(); ++i)
		{
			uint8_t owner = _state.routeOwner[i];
			if (owner != NO_OWNER)
				m_Routes[i].owner = &_state.player[owner];
		}
	}

	template<typename Tr, typename Tc, typename Tl>
	std::vector<Tl*> Map<Tr, Tc, Tl>::dijkstra(CityName _start, std::function<bool(const Tl&)> _end, std::function<int(const Tl&)> _cost)
	{
		using namespace std;

		array<pair<Tc*, size_t>, CN_NONE> v;
		v.fill({ nullptr, ~0u });
		v[_start].second = 0;

		auto cmp = [&](const Tc* _a, const Tc* _b) { return v[_a->name].second > v[_b->name].second; };
		vector<Tc*> q;
		q.push_back(&m_Cities[_start]);

		while (!q.empty())
		{
			Tc* node = q.front();
			pop_heap(q.begin(), q.end(), cmp);
			q.pop_back();

			for (auto& it : node->routes)
			{
				int cost = _cost(it);
				if (cost < 0)
					continue;

				size_t dist = v[node->name].second + cost;

				auto& dest = v[it.dest.name];
				if (dist < dest.second)
				{
					dest.second = dist;
					dest.first = node;

					auto pos = find(q.begin(), q.end(), &it.dest);
					if (pos == q.end())
					{
						q.push_back(&it.dest);
						push_heap(q.begin(), q.end(), cmp);
					}
					else
					{
						make_heap(q.begin(), q.end(), cmp);
					}
				}

				if (_end(it))
				{
					vector<Tl*> path;

					CityName curCity = it.dest.name;
					while (curCity != _start)
					{
						Tl* link = nullptr;
						for (auto& it : v[curCity].first->routes)
							if (it.dest.name == curCity)
							{
								int newCost = _cost(it);
								if (newCost < 0) continue;
								if (!link)
									link = &it;
								else if (newCost < _cost(*link))
									link = &it;
							}
						path.push_back(link);
						curCity = v[curCity].first->name;
					}

					reverse(path.begin(), path.end());
					return path;
				}
			}
		}
		return vector<Tl*>();
	}

	template<typename Tr, typename Tc, typename Tl>
	std::vector<Tl*> Map<Tr, Tc, Tl>::longestPath(CityName _start, const Player & _player)
	{
		using namespace std;

		vector<Tl*> maxPath;
		size_t maxPathLen = 0;

		vector<Tl*> path;
		size_t pathLen = 0;

		array<bool, NUM_ROUTES> inPath = {};
		function<void(Tc&)> func = [&](Tc& _city) {

			for (Tl& link : _city.routes)
			{
				size_t idx = link.route.def->id - 1;
				if (inPath[idx] || !link.route.owner || link.route.owner->id != _player.id)
					continue;

				inPath[idx] = true;
				path.push_back(&link);
				pathLen += link.route.def->length;

				if (pathLen > maxPathLen)
				{
					maxPath = path;
					maxPathLen = pathLen;
				}

				func(link.dest);

				inPath[idx] = false;
				path.pop_back();
				pathLen -= link.route.def->length;
			}

		};
		func(m_Cities[_start]);
		return maxPath;
	}
}
