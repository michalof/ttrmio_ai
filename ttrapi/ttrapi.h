#pragma once
#include "ttrdefinitions.h"

#include <string>
#include <deque>
#include <memory>
#include <array>
#include <queue>
#include <vector>
#include <functional>
#include <chrono>
#include <sstream>
#include <fstream>
#include <mutex>
#include <condition_variable>

namespace ttrapi
{
	class connection_failure : public std::runtime_error
	{
	public:
		connection_failure(std::string _msg) : std::runtime_error(_msg) {}
	};

	class protocol_error : public std::runtime_error
	{
	public:
		protocol_error(std::string _msg) : std::runtime_error(_msg) {}
	};

	class rule_violation : public std::logic_error
	{
	public:
		rule_violation(std::string _msg) : std::logic_error(_msg) {}
	};

	class timeout : public rule_violation
	{
	public:
		timeout() : rule_violation("timeout") {}
	};

	class Pipe
	{
	public:
		enum Name {
			CLIENT = 0,
			SERVER = 1
		};

		void send(Name _to, std::string _str);
		bool recv(Name _from, std::string& _str);
	private:
		std::mutex				mut;
		std::condition_variable cond;
		std::queue<std::string>	buf[2];
	};

	class SocketWrapper
	{
	public:
		SocketWrapper();
		~SocketWrapper();
		SocketWrapper(const SocketWrapper&) = delete;
		SocketWrapper(SocketWrapper&&);

		bool listen(uint16_t _port);
		unsigned int socket() const { return socket_; }
	private:
		unsigned int socket_;
	};

	class Connection
	{
	public:
		Connection(Pipe* _pipe, size_t _myId);
		Connection(std::string _playerName, const char* _hostname = "localhost", uint16_t _port = 13000);
		~Connection();

		void send(const char* _cmd);
		void send(const std::ostringstream& _str);

		std::string recv(int _packetType) { return recv(_packetType, _packetType); }
		std::string recv(int _packetTypeMin, int _packetTypeMax, int* _code = nullptr);

		size_t playerId() const { return m_PlayerId; }
		const GameState& gameState() const { return m_State; }
		const Player& playerState() const { return m_State.player[m_PlayerId]; }
		size_t currentRound() const { return m_State.round; }
	protected:
		void connect(std::string& _playerName, const char* _hostname, uint16_t _port);
		void updateGameState(const std::string& _jsonState);
		void updateTrainCards(const std::string& _cards);

	private:
		std::deque<std::pair<int, std::string> >	m_Packets;
		
		Pipe*			m_Pipe = nullptr;
		unsigned int	m_Sock = 0;
		size_t			m_PlayerId = 0;
		std::string		m_Hash;
		GameState		m_State;
	};

	struct TicketDecision : TicketDef
	{
		bool keep = true;
	};

	class Token
	{
	public:
		Token(std::shared_ptr<Connection> _conn);

		float timeLeft() const;

		void drawCoverdTrainCard();
		void drawOpenTrainCard(size_t _spot);
		void drawDestinationTickets(std::function<void(std::vector<TicketDecision>& _tickets)> _callback = [](std::vector<TicketDecision>& _tickets) {});
		void buildRoute(const RouteDef& _route, TrainCardColor _color, std::function<bool(size_t _extraCost)> _callback = [](size_t) { return true; });
		void buildStation(CityName _city, TrainCardColor _color);

	private:
		std::shared_ptr<Connection>	m_Conn;
		std::chrono::system_clock::time_point m_StartTime;
		float m_Timeout;
	};

	class GameServer
	{
		enum TurnState
		{
			TS_IDLE,
			TS_WAIT,
			TS_TUNNLE,
			TS_FIRST_CARD,
			TS_DEST_CARD,
			TS_DONE
		};

		struct ClientInfo
		{
			Pipe*			pipe = nullptr;
			unsigned int	sock = 0;
			std::string		name;
		};

	public:
		GameServer();
		~GameServer();

		void enableNetworkLogging(const char* _filename);
		bool listen(uint16_t _port);
		void listen(std::shared_ptr<SocketWrapper> _sock) { m_Sock = _sock; }

		std::string waitForPlayer();
		size_t addLocalPlayer(Pipe* _pipe);
		
		void close();

		// call until false
		bool run();
		bool aiResponsive() const;

		std::vector<int> points() const;

		Player& player() { return m_State.player[m_Turn % m_State.player.size()]; }
		ServerGameState& state() { return m_State; }
	protected:
		void readSocketLine(ClientInfo& _sock, std::string& _buf, time_t _timeoutMs = 0);
		void sendTurnMsg();
		void sendErrMsg(size_t _pid, int _err);
		void readCmdMsg();
		void writeMsg(ClientInfo& _sock, std::ostringstream& _msg);

		void clientDeTi(size_t _pid, std::vector<std::string>& _cmd);
		void clientDTDe(size_t _pid, std::vector<std::string>& _cmd);
		void clientOpTC(size_t _pid, std::vector<std::string>& _cmd);
		void clientCoTC(size_t _pid, std::vector<std::string>& _cmd);
		void clientRoBu(size_t _pid, std::vector<std::string>& _cmd);
		void clientRoDe(size_t _pid, std::vector<std::string>& _cmd);
		void clientStBu(size_t _pid, std::vector<std::string>& _cmd);
		void clientDefaultMove(size_t _pid);

		std::string jsonPlayerData(size_t _pid);
		std::string jsonGameData();

	private:

		std::shared_ptr<SocketWrapper>	m_Sock;
		std::vector<ClientInfo>			m_Clients;
		ServerGameState					m_State;

		time_t							m_TurnStart = 0;
		size_t							m_Turn = 0;
		size_t							m_LastTurn = 0;
		TurnState						m_TurnState = TS_IDLE;
		std::string						m_TurnHash;

		std::vector<TicketDef>			m_TurnTickets;
		size_t							m_TurnRouteId = 0;
		size_t							m_TurnRouteExtraCost = 0;
		TrainCardColor					m_TurnRouteColor = TCC_NONE;

		std::ofstream					m_NetLog;
	};

}
