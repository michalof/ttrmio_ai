#include "../ttrapi/ttrapi.h"
#include "../ttrapi/ttrgraph.h"
#include <iostream>
#include <thread>

#include <tclap/CmdLine.h>
#include <tclap/ValueArg.h>

using namespace ttrapi;
using namespace std;

static void aiThink(shared_ptr<Connection> _conn)
{
	Token token(_conn);

	if (_conn->currentRound() == 0)
	{
		token.drawDestinationTickets();
		return;
	}

	const TicketDef* ticket = nullptr;
	vector<Route*> ticketPath;

	Map<> net;
	net.updateOwner(_conn->gameState());
	for (auto& it : _conn->playerState().ticketCards)
	{
		vector<CityLink*> path = net.dijkstra(it.city[0], it.city[1], [&](const CityLink& _l) {
			if (_l.route.owner && _l.route.owner->id == _conn->playerId())
				return 0;
			else if (!_l.route.available())
				return -1;
			else
				return (int)_l.route.def->length;
		});
		for (auto it : path)
			ticketPath.push_back(&it->route);
	}

	bool allDone = true;
	for (auto it : ticketPath)
		allDone &= it->owner != nullptr;

	if (allDone)
	{
		token.drawDestinationTickets();
		return;
	}

	size_t numRoutes = ticketPath.size();
	for (size_t i = 0; i < numRoutes; ++i)
		if (ticketPath[i]->twin)
			ticketPath.push_back(ticketPath[i]->twin);

	for (auto& l : ticketPath)
	{
		TrainCardColor color = l->canBuild(_conn->playerState(), l->def->tunnle ? 1 : 0);
		if (color != TCC_NONE)
		{
			token.buildRoute(*l->def, color);
			return;
		}
	}

	token.drawCoverdTrainCard();
	token.drawCoverdTrainCard();
}

static void runAi()
{
	try
	{
		shared_ptr<Connection> conn = make_shared<Connection>("dummy", "127.0.0.1");

		while (true)
		{
			try {
				aiThink(conn);
			}
			catch (rule_violation e)
			{
				cerr << "dummy: rule violation " << e.what() << endl;
			}
		}
	}
	catch (connection_failure)
	{}
}

int main(int argc, char* argv[])
{
	using namespace TCLAP;

	CmdLine cmd("ticket to ride game server for AI challenge", ' ', "0.2");
	ValueArg<uint32_t> seedArg("s", "seed", "initial game seed", false, 1337, "uint32", cmd);
	ValueArg<uint32_t> timeoutArg("t", "timeout", "timeout in ms", false, 1000, "uint32", cmd);
	ValueArg<uint32_t> portArg("p", "port", "tcp port number", false, 13000, "uint32", cmd);
	ValueArg<uint32_t> numPlayerArg("n", "num-player", "number of players", false, 1, "uint32", cmd);
	ValueArg<uint32_t> numGamesArg("g", "num-games", "number of games played", false, 1, "uint32", cmd);
	ValueArg<string> netlogArg("l", "netlog", "log network pakets to file", false, "net.log", "string", cmd);
	ValueArg<string> resultFileArg("r", "result", "filename for result csv", false, "", "string", cmd);
	SwitchArg longRoutesArg("L", "long-routes", "enable long route rule", cmd, false);

	UnlabeledMultiArg<string> aiArg("ai", "command to be launched for each game. use dummy for internal ai.", false, "string", cmd);

	try {
		cmd.parse(argc, argv);
	}
	catch (TCLAP::ArgException &e)
	{
		cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
		return 1;
	}

	ofstream csv;
	if (resultFileArg.isSet())
		csv.open(resultFileArg.getValue().c_str(), ios_base::app);

	auto listenSocket = make_shared<SocketWrapper>();
	while (!listenSocket->listen(portArg.getValue()))
	{
		cerr << "waiting for port " << portArg.getValue() << " to be available" << endl;
		this_thread::sleep_for(chrono::seconds(1));
	}
	

	for (uint32_t g = 0; g < numGamesArg.getValue(); ++g)
	{
		GameServer s;
		s.listen(listenSocket);

		vector<string> names;
		vector<thread> ai;
		for (const string& aicmd : aiArg.getValue())
		{
			ai.emplace_back([=]() {
				if (aicmd == "dummy")
					runAi();
				else
					std::system(aicmd.c_str());
			});
			names.push_back(s.waitForPlayer());
			cout << names.back() << " connected local" << endl;
		}

		while (s.state().player.size() < numPlayerArg.getValue())
		{
			string name = s.waitForPlayer();
			if (!name.empty())
			{
				names.push_back(name);
				cout << name << " connected" << endl;
			}
		}

		if (s.state().player.empty())
			return 0;

		uint32_t gameSeed = seedArg.getValue() + g;
		s.state().shuffle(gameSeed, longRoutesArg.getValue());
		s.state().timeout = (float)timeoutArg.getValue() / 1000;

		if (netlogArg.isSet())
			s.enableNetworkLogging(netlogArg.getValue().c_str());

		while (s.run() && s.aiResponsive());
		vector<int> points;
		if (s.aiResponsive())
		{
			csv << gameSeed << '\t' << s.state().round << '\t';
			points = s.points();
		}
		else
		{
			cerr << gameSeed << ": ai's unresponsive" << endl;
			points.resize(s.state().player.size(), 0);
		}
		
		vector<string> namesSorted = names;
		sort(namesSorted.begin(), namesSorted.end());
		for (string& name : namesSorted)
		{
			ptrdiff_t id = find(names.begin(), names.end(), name) - names.begin();
			csv << name << '\t' << points[id] << '\t';
		}
		csv << endl;

		s.close();
		for (thread& t : ai)
			t.join();
	}

	return 0;
}
