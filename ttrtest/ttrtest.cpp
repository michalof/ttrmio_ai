#include "../ttrapi/ttrapi.h"
#include "../ttrapi/ttrgraph.h"
#include <iostream>
#include <thread>

using namespace ttrapi;
using namespace std;

static void tryBuildRoute(Token _token, string _testCase, bool _expectError, const RouteDef & _route, TrainCardColor _color, std::function<bool(size_t _extraCost)> _callback)
{
	bool ruleViolated = false;

	try
	{
		_token.buildRoute(_route, _color, _callback);
	}
	catch (rule_violation) { ruleViolated = true; }

	if (ruleViolated != _expectError)
		cout << _testCase;
}

static void tryBuildStation(Token _token, string _testCase, bool _expectError, CityName _city, TrainCardColor _color)
{
	bool ruleViolated = false;

	try
	{
		_token.buildStation(_city, _color);
	}
	catch (rule_violation) { ruleViolated = true; }

	if (ruleViolated != _expectError)
		cout << _testCase;
}

static void aiThink(shared_ptr<Connection> _conn)
{
	Token token(_conn);

	if (_conn->currentRound() == 0)
	{
		token.drawDestinationTickets();
		return;
	}

	const TicketDef* ticket = nullptr;
	vector<Route*> ticketPath;

	Map net;
	net.updateOwner(_conn->gameState());
	for (auto& it : _conn->playerState().ticketCards)
	{
		vector<CityLink*> path = net.dijkstra(it.city[0], it.city[1], [&](const CityLink& _l) {
			if (_l.route.owner && _l.route.owner->id == _conn->playerId())
				return 0;
			else if (!_l.route.available())
				return -1;
			else
				return (int)_l.route.def->length;
		});
		for (auto it : path)
			ticketPath.push_back(&it->route);
	}

	bool allDone = true;
	for (auto it : ticketPath)
		allDone &= it->owner != nullptr;

	if (allDone)
	{
		token.drawDestinationTickets();
		return;
	}

	size_t numRoutes = ticketPath.size();
	for (size_t i = 0; i < numRoutes; ++i)
		if (ticketPath[i]->twin)
			ticketPath.push_back(ticketPath[i]->twin);

	for (auto& l : ticketPath)
	{
		TrainCardColor color = l->canBuild(_conn->playerState(), l->def->tunnle ? 1 : 0);
		if (color != TCC_NONE)
		{
			token.buildRoute(*l->def, color);
			return;
		}
	}

	token.drawCoverdTrainCard();
	token.drawCoverdTrainCard();
}

static void aiThinkFoo(shared_ptr<Connection> _conn)
{
	Token token(_conn);
	size_t round = _conn->currentRound();
	bool rule_violation_occured;

	if (round == 0)
	{
		try
		{
			token.drawDestinationTickets(); // TODO Alle zurücklegen
		}
		catch (rule_violation) { }

		return;
	}
	
	else if (round == 1)
	{
		rule_violation_occured = false;
		try
		{
			token.buildRoute(g_Routes[23], TCC_WHITE);
		}
		catch (rule_violation) { rule_violation_occured = true; };
	}
}

static void aiThinkBar(shared_ptr<Connection> _conn)
{
	Token token(_conn);
	size_t round = _conn->currentRound();
}

static void ai(string _name, void (*_aiThink)(shared_ptr<Connection>))
{
	try
	{
		shared_ptr<Connection> conn = make_shared<Connection>(_name);

		while (true)
		{
			try
			{
				_aiThink(conn);
			}
			catch (rule_violation e)
			{
				cerr << "rule violation " << e.what() << endl;
			}
		}
	}
	catch (connection_failure e)
	{
		cerr << "network error: " << e.what() << endl;
	}
}

int main()
{
	GameServer s;

	if (!s.listen(13000))
	{
		cerr << "listening failed" << endl;
		return 1;
	}

	thread aiFoo(ai, "foo", aiThinkFoo);
	thread aiBar(ai, "bar", aiThinkBar);
	
	while (s.state().player.size() != 2)
	{
		string name = s.waitForPlayer();
		cout << name << " connected" << endl;
	}
	s.state().shuffle(42);

	while (s.run());
	s.close();
	cout << "game finished" << endl;

	aiFoo.join();
	aiBar.join();
}
